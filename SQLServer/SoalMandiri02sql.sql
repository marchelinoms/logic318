--1.Tampilkan nama barang, harga barang dan tambahkan kolom dengan keterangan (jika diatas rata2 maka upper, jika dibawah rata2 maka lower)
select 
	Nama, Harga, 
	case when Harga > (select avg(Harga) from Product) then 'Upper' 
	else 'Lower' end Keterangan
from
	Product
order by Keterangan DESC, Harga DESC, Nama

--2.Tampilkan Produk yang terjual di bulan dan hari yang sama
SELECT Distinct *
FROM vwDayMonthProduk dayPro
JOIN
	(
		SELECT *
		FROM vwDayMonthProduk
	) sub
ON dayPro.Hari = sub.Hari AND dayPro.Bulan = sub.Bulan AND dayPro.KodeProduct <> sub.KodeProduct

CREATE VIEW vwDayMonthProduk
AS
SELECT KodeProduct, DAY(SellingDate) Hari, MONTH(SellingDate) Bulan 
FROM 
Selling

--3.Tampilkan Frekuensi terjualnya suatu produk dengan menampilkan tanggal dan quantitynya
SELECT KodeProduct,SUM(Quantity) Quantity,DAY(SellingDate) Tanggal, COUNT(DAY(SellingDate))Kali_Terjual
FROM Selling
GROUP BY KodeProduct, DAY(SellingDate)
ORDER BY Quantity,DAY(SellingDate)