create database klinikMega
GO
use klinikMega

create table Pasien(
ID_Pasien varchar(5) primary key,
NIK varchar(25) not null,
Nama_Depan varchar(15) not null,
Nama_Belakang varchar(15), 
TglLahir varchar(200) not null)

create table Dokter(ID_Dokter int identity(1,1),
[STR] varchar(10) primary key,
Nama_Depan varchar(15) not null, 
Nama_Belakang varchar(15) null,
Spesialis varchar(15));

create table Rekam_Medis(
ID_Rekam_Medis varchar(5) primary key, 
ID_Pasien varchar(5), 
[STR] varchar(10), 
Diagnosa varchar(500) null);

create table Resep(
ID_Resep varchar(5) primary key,
Tanggal_Resep datetime, 
ID_Pasien varchar(5),
ID_Obat varchar(5), 
Quantity int not null);

create table Obat(
ID_Obat varchar(5) primary Key, 
Nama_Obat varchar(15) not null,
Stok int not null, 
Harga money not null,
Tanggal_Kadaluarsa datetime not null);

create table Invoice(
ID_Invoice varchar(5) primary key, 
Tanggal_Invoice datetime not null,
ID_Pasien varchar(10), 
Biaya_Perawatan money not null,  
Total_Pembayaran money not null);

