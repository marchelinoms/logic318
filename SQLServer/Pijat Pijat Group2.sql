create database PijatTerserah
GO
use PijatTerserah

create table Grade(ID_Grade varchar(5) primary key,
type varchar(15) not null);

create table Provinsi(ID_Provinsi varchar(5) primary key,
Nama varchar(35) not null);

create table Kota(ID_Kota varchar(5) primary key,
Nama varchar(35) not null, ID_Provinsi varchar(5) foreign key references Provinsi(ID_Provinsi));

create table Member(ID_Member varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
Alamat varchar(500) null, ID_Kota varchar(5) foreign key references Kota(ID_Kota),
ID_Grade varchar(5) foreign key references Grade(ID_Grade));

create table Job_Title(ID_Job varchar(5) primary key,
Title varchar(25) not null);

create table Employee(ID_Employee varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
ID_Job varchar(5) foreign key references Job_Title(ID_Job));

create table Booking(ID_Booking varchar(4) primary key,
Tanggal DateTime not null,
ID_Member varchar(5) foreign key references Member(ID_Member));

--create table Type_Message(ID_Type_Message varchar(5) primary key,
--ID_Booking varchar(4) foreign key references Booking(ID_Booking),
--Tipe varchar(25) not null);

create table Massage(ID_Massage varchar(5) primary key,
Tipe varchar(25) not null, Harga money not null);

create table Goods(ID_Goods varchar(5) primary key,
Tipe varchar(25) not null,Quantity int not null, Harga money not null);

create table BookingMessage(ID_Booking_Message varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Massage varchar(5) foreign key references Massage(ID_Massage),
Masanger varchar(5) foreign key references Employee(ID_Employee),
Harga money not null);

create table BookingGoods(ID_Booking_Goods varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Goods varchar(5) foreign key references Goods(ID_Goods),
Quantity int not null, Harga money not null);

create table Invoice(ID_Invoice varchar(9) primary key,
Tanggal_Invoice datetime not null,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
Kasir varchar(5) foreign key references Employee(ID_Employee));

insert into Provinsi(ID_Provinsi,Nama)
values
('PV002','DKI Jakarta'),('PV003','Jawa Barat');

insert into Kota(ID_Kota, Nama, ID_Provinsi)
values('KT002','Tanggerang','PV001'),('KT003','Serang','PV001'),('KT004','Jakarta Utara','PV002'),('KT005','Jakarta Selatan','PV002'),('KT006','Jakarta Barat','PV002'),('KT007','Jakarta Timur','PV002'),('KT008','Bandung','PV003');

insert into Grade(ID_Grade,[type])
values('GD002','Silver'),('GD003','Bronze'),('GD004','Platinum');

insert into Member(ID_Member, Nama_Depan, Nama_Belakang, Alamat, ID_Kota, ID_Grade)
values
('M0001','Bayu','Ananda','Jl. Pancasila','KT001','GD001'),('M0002','Irfan','Hakim','Jl. Satrio','KT005','GD001'),
  ('M0003','Imam','Syafii','Jl. Kebangsaan','KT007','GD002'),('M0004','Rika','Damayanti','Jl. Rasa Sayang','KT006','GD002'),('M0005','Tono','Ananda','Jl. Manggis','KT002','GD003'),
	('M0006','Raditya','Dika','Jl.inAjaDulu','KT001','GD004'),
	('M0007','Raditya','Oloan','Jl.jalan','KT008','GD004');

insert into Job_Title(ID_Job, Title)
values('JB001','Massager'),('JB002','Kasir');

insert into Employee(ID_Employee, Nama_Depan, Nama_Belakang,ID_Job)
values
('E0003','Wahyu',null,'JB001'),
('E0004','Bagas',null,'JB001'),
('E0005','Nurul','Fitriani','JB001'),
('E0006','Jamal','Wiwoho','JB001'),
('E0007','Nana','Kumala','JB001'),
('E0008','Ningsih',null,'JB001'),
('E0009','Agus',null,'JB001'),
('E0010','Yopie',null,'JB002');

insert into Booking(ID_Booking, Tanggal, ID_Member)
values('B098','20230104','M0982');

insert into Massage(ID_Massage, Tipe, Harga)
values('M0001','Full Body',120000);

insert into BookingMessage(ID_Booking_Message, ID_Booking, ID_Massage, Masanger, Harga)
values('BM001','B098','M0001','E0001',120000);

insert into Goods(ID_Goods, Tipe, Quantity, Harga)
values('G0001','Miyak Terapi',60,25000),('G0002','Wedang Jahe',100,15000),
('G0003','Handuk',120,46500);

insert into BookingGoods(ID_Booking_Goods, ID_Booking, ID_Goods, Quantity, Harga)
values('BG001','B098','G0001',1,25000),('BG002','B098','G0002',1,15000),
('BG003','B098','G0003',1,46500);

insert into Invoice(ID_Invoice, Tanggal_Invoice, ID_Booking, Kasir)
values
('M23010923','20230105','B098','E0002'),

create view DataGathering
as
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi], gd.[type] [Grade], 
'Masseuse' as [type], mas.Tipe [detailType], em.Nama_Depan [Masanger], boma.Harga [Price], 6000 as [Service]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
union 
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi],gd.[type] [Grade],
'Goods' as [type], god.Tipe [detailType], '' as [Masanger],bogo.Harga [Price], 0 as [Service]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingGoods bogo on bo.ID_Booking=bogo.ID_Booking
join Goods god on god.ID_Goods=bogo.ID_Goods
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
--00.Rekapitulasi
select format([Date],'dd-MM-yy') [Dates],Reff,[BookNo],FORMAT(Tgl,'dd-MM-yy') Tanggal,MemberId,NamaCustomer,Alamat, Kota, Provinsi,Grade,
[type], detailType, Masanger,em.Nama_Depan as Kasir,Price,[Service],CAST(Price*0.11 as int) Tax,
cast(Price+[Service]+(Price*0.11) as int) Payment
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir
--01.Tampilkan Masseus dengan pelanggan terbanyak
--03 Tampilkan service massage yang paling sedikit peminat
--04 Tampilkan jumlah service setiap bulannya.
--05 Tampilkan tahun bulan penjualan terbanyak
--06 Tampilkan masing-masing total pendapatan service/massage & product/goods.
--07 Dalam meningkatkan minat pelanggan, diadakan promo untuk para member dan mendapat potongan.
--		Bronze 2.5% untuk service tertentu
--		Silver 5% untuk service tertentu
--		Gold 5% untuk service & product tertentu
--		Platinum 8% untuk service & product tertentu
--	a. Tambahkan tabel dan/atau kolom diskon per grade dan product.
--	b. Tampilkan transaksi dengan diskon untuk customer sesuai grade-nya.
--08 Tambahkan jenis kelamin untuk member/pelanggan & untuk Masseus, Jenis kelamin char(1) L/P
ALTER TABLE Member
ADD Jenis_Kelamin CHAR(1) Check(Jenis_Kelamin ='L' OR Jenis_Kelamin ='P')
ALTER TABLE Employee
ADD Jenis_Kelamin CHAR(1) Check(Jenis_Kelamin ='L' OR Jenis_Kelamin ='P')
--09 Tampilkan jumlah customer per Masseuse dan bonus dimana Jumlah customer 5-10 mendapat 5% dari harga service, sedangkan >10 mendapat 8% dari harga serice.
--10 Tampilkan tren jumlah customer harian dengan nama hari (Sunday-Saturday) segala waktu.
--select *
--from Invoice iv
--join Booking bo on iv.ID_Booking=bo.ID_Booking
--join Employee em on iv.Kasir=em.ID_Employee
--join Job_Title joti on joti.ID_Job=em.ID_Job

--join Member mem on mem.ID_Member=bo.ID_Member
--join Grade gd on gd.ID_Grade=mem.ID_Grade
--join Kota kt on kt.ID_Kota=mem.ID_Kota
--join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
