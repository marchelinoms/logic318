CREATE DATABASE DBPenerbit
GO
USE DBPenerbit
--No.1
CREATE TABLE tblPengarang
(
ID INT IDENTITY(1,1) NOT NULL,
Kd_Pengarang VARCHAR(7) PRIMARY KEY,
Nama VARCHAR(30) NOT NULL,
Alamat VARCHAR(80) NOT NULL,
Kota VARCHAR(15) NOT NULL,
Kelamin VARCHAR(1) NOT NULL
)
--No.2
INSERT INTO tblPengarang (Kd_Pengarang,Nama,Alamat,Kota,Kelamin) VALUES
('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti', 'Jl. Durian 15','Solo','W'),
('P0005', 'Amir','Jl. Gajah 33', 'Kudus','P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
('P0008', 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')
GO
--No.3
CREATE VIEW vwPengarang AS
(
	SELECT Kd_Pengarang,Nama,Kota
	FROM tblPengarang
)
GO
--No.3a
SELECT Kd_Pengarang,Nama
FROM tblPengarang
ORDER BY Nama
--No.3b
SELECT Kd_Pengarang,Nama,Kota
FROM tblPengarang
ORDER BY Kota
--No.3c
SELECT COUNT(Kd_Pengarang) Jumlah_Pengarang
FROM tblPengarang
--No.3d
SELECT Kota, COUNT(Kd_Pengarang) Jumlah
FROM tblPengarang
GROUP BY Kota
--No.3e
SELECT Kota, COUNT(Kd_Pengarang) Jumlah
FROM tblPengarang
GROUP BY Kota
HAVING COUNT(Kd_Pengarang) > 1
--No.3f
SELECT MAX(Kd_Pengarang) Terbesar, MIN(Kd_Pengarang) Terkecil
FROM tblPengarang
--No.4
CREATE TABLE tblGaji
(
ID INT PRIMARY KEY IDENTITY(1,1),
Kd_Pengarang VARCHAR(7) NOT NULL,
Nama VARCHAR(30) NOT NULL,
Gaji DECIMAL(18,4) NOT NULL
)
--No.5
INSERT INTO tblGaji(Kd_Pengarang,Nama,Gaji) VALUES
('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)
--No.5a
SELECT MAX(Gaji) Tertinggi, MIN(Gaji) Terendah
FROM tblGaji
--No.5b
SELECT *
FROM tblGaji
WHERE Gaji > 600000
--No.5c
SELECT SUM(Gaji)Jumlah_Gaji
FROM tblGaji
--No.5d
SELECT Kota,SUM(Gaji)Jumlah_Gaji
FROM tblGaji gaji
JOIN tblPengarang peng on gaji.Kd_Pengarang = peng.Kd_Pengarang
GROUP BY Kota
--No.5e
SELECT *
FROM tblPengarang
WHERE Kd_Pengarang BETWEEN 'P0001' AND 'P0006'
--No.5f
SELECT *
FROM tblPengarang
WHERE Kota IN ('Yogya','Solo','Magelang')
--No.5g
SELECT *
FROM tblPengarang
WHERE Kota NOT IN ('Yogya')
--No.5h
	--A.
		SELECT *
		FROM tblPengarang
		WHERE Nama LIKE 'A%'
	--B.
		SELECT *
		FROM tblPengarang
		WHERE Nama LIKE '%i'
	--C.
		SELECT *
		FROM tblPengarang
		WHERE Nama LIKE '__a%'
	--D.
		SELECT *
		FROM tblPengarang
		WHERE Nama NOT LIKE '%n'
--No.5i
SELECT *
FROM tblPengarang,tblGaji
WHERE tblGaji.Kd_Pengarang = tblPengarang.Kd_Pengarang
--No.5j
SELECT DISTINCT Kota
FROM tblGaji gaji
JOIN tblPengarang peng on peng.Kd_Pengarang = gaji.Kd_Pengarang
WHERE Gaji < 1000000
--No.5k
ALTER TABLE tblPengarang
ALTER COLUMN Kelamin VARCHAR(10)
--No.5l
ALTER TABLE tblPengarang
ADD Gelar VARCHAR(12)
--No.5m
UPDATE tblPengarang SET Alamat = 'Jl. Cendrawasih 65' ,Kota = 'Pekanbaru' 
WHERE Kd_Pengarang = 'P0002'