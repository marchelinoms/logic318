create database PijatTerserah
use PijatTerserah
--drop database PijatTerserah

create table Grade(ID_Grade varchar(5) primary key,
type varchar(15) not null);

create table Provinsi(ID_Provinsi varchar(5) primary key,
Nama varchar(35) not null);

create table Kota(ID_Kota varchar(5) primary key,
Nama varchar(35) not null, ID_Provinsi varchar(5) foreign key references Provinsi(ID_Provinsi));

create table Member(ID_Member varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
Alamat varchar(500) null, ID_Kota varchar(5) foreign key references Kota(ID_Kota),
ID_Grade varchar(5) foreign key references Grade(ID_Grade));

create table Job_Title(ID_Job varchar(5) primary key,
Title varchar(25) not null);

create table Employee(ID_Employee varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
ID_Job varchar(5) foreign key references Job_Title(ID_Job));

create table Booking(ID_Booking varchar(4) primary key,
Tanggal DateTime not null,
ID_Member varchar(5) foreign key references Member(ID_Member));

--create table Type_Message(ID_Type_Message varchar(5) primary key,
--ID_Booking varchar(4) foreign key references Booking(ID_Booking),
--Tipe varchar(25) not null);

create table Massage(ID_Massage varchar(5) primary key,
Tipe varchar(25) not null, Harga money not null);

create table Goods(ID_Goods varchar(5) primary key,
Tipe varchar(25) not null,Quantity int not null, Harga money not null);

create table BookingMessage(ID_Booking_Message varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Massage varchar(5) foreign key references Massage(ID_Massage),
Masanger varchar(5) foreign key references Employee(ID_Employee),
Harga money not null);

create table BookingGoods(ID_Booking_Goods varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Goods varchar(5) foreign key references Goods(ID_Goods),
Quantity int not null, Harga money not null);

create table Invoice(ID_Invoice varchar(9) primary key,
Tanggal_Invoice datetime not null,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
Kasir varchar(5) foreign key references Employee(ID_Employee));

insert into Provinsi(ID_Provinsi,Nama)
values('PV001','Banten'),('PV002','DKI Jakarta'),('PV003','Jawa Barat');

insert into Kota(ID_Kota, Nama, ID_Provinsi)
values('KT001','Tanggerang Selatan','PV001'),('KT002','Tanggerang','PV001'),('KT003','Serang','PV001'),
('KT004','Jakarta Utara','PV002'),('KT005','Jakarta Selatan','PV002'),
('KT006','Jakarta Barat','PV002'),('KT007','Jakarta Timur','PV002'),
('KT008','Bandung','PV003');

insert into Grade(ID_Grade,[type])
values('GD001','Gold'),('GD002','Silver'),('GD003','Bronze'),('GD004','Platinum');

insert into Member(ID_Member, Nama_Depan, Nama_Belakang, Alamat, ID_Kota, ID_Grade)
values('M0982','Bayu',null,'Jl. Garuda','KT001','GD001'),
('M0001','Bayu','Ananda','Jl. Pancasila','KT001','GD001'),
('M0002','Irfan','Hakim','Jl. Satrio','KT005','GD001'),
  ('M0003','Imam','Syafii','Jl. Kebangsaan','KT007','GD002'),
  ('M0004','Rika','Damayanti','Jl. Rasa Sayang','KT006','GD002'),
  ('M0005','Tono','Ananda','Jl. Manggis','KT002','GD003'),
  ('M0006','Ahmad','Bakhri','Jl. Daan Mongot','KT008','GD001'),
  ('M0007','Nurul',null,'Jl. Cempreng','KT001','GD002'),
  ('M0008','Guspian',null,'Jl. Rasa Vanila','KT004','GD002'),
  ('M0009','Charli','Caplin','Jl. ST 12','KT002','GD003');

insert into Job_Title(ID_Job, Title)
values('JB001','Massager'),('JB002','Kasir');

insert into Employee(ID_Employee, Nama_Depan, Nama_Belakang,ID_Job)
values('E0001','Wahyu',null,'JB001'),('E0002','Nino',null,'JB002'),
('E0003','Wahyu',null,'JB001'),('E0004','Bagas',null,'JB001'),
('E0005','Nurul','Fitriani','JB001'),('E0006','Jamal','Wiwoho','JB001'),
('E0007','Nana','Kumala','JB001'),('E0008','Ningsih',null,'JB001'),
('E0009','Agus',null,'JB001'),('E0010','Yopie',null,'JB002');

insert into Booking(ID_Booking, Tanggal, ID_Member)
values('B098','20230104','M0982'),('B001','20230104','M0001'),
('B002','20230105','M0002'),('B003','20230210','M0003'),
('B004','20230811','M0004'),('B005','20230813','M0005'),
('B006','20230909','M0006'),('B007','20240102','M0007'),
('B008','20240109','M0008'),('B009','20240202','M0009');

insert into Massage(ID_Massage, Tipe, Harga)
values('ME001','Full Body',120000),('ME002','Half Body',60000),
('ME003','Totok',50000),('ME004','Spa',180000);

insert into BookingMessage(ID_Booking_Message, ID_Booking, ID_Massage, Masanger, Harga)
values('BM001','B098','ME001','E0001',120000),('BM002','B001','ME001','E0007',120000),
('BM003','B002','ME004','E0008',180000),('BM004','B003','ME001','E0001',120000),
('BM005','B004','ME003','E0004',50000),('BM006','B005','ME001','E0001',120000),
('BM007','B006','ME002','E0009',60000),('BM008','B007','ME003','E0003',50000),
('BM009','B008','ME003','E0008',50000),('BM010','B009','ME001','E0001',120000);

insert into Goods(ID_Goods, Tipe, Quantity, Harga)
values('G0001','Miyak Terapi',60,25000),('G0002','Wedang Jahe',100,15000),
('G0003','Handuk',120,46500),('G0004','Oil Essence',170,81000),
('G0005','Jus',70,18000),('G0006','Salep',22,10000),
('G0007','Minyak Kayu P',110,6000),('G0008','Lasegar',200,15000);

insert into BookingGoods(ID_Booking_Goods, ID_Booking, ID_Goods, Quantity, Harga)
values('BG001','B098','G0001',1,25000),('BG002','B098','G0002',1,15000),
('BG003','B098','G0003',1,46500),('BG004','B002','G0005',2,36000),
('BG005','B004','G0005',1,18000),('BG006','B004','G0006',2,20000),
('BG007','B005','G0003',1,46500),('BG008','B005','G0004',2,162000),
('BG009','B008','G0001',3,75000),('BG010','B009','G0001',2,50000);

insert into Invoice(ID_Invoice, Tanggal_Invoice, ID_Booking, Kasir)
values('M23010923','20230105','B098','E0002'),('M23010924','20230105','B001','E0002'),
('M23010925','20230106','B002','E0002'),('M23010926','20230210','B003','E0002'),
('M23010927','20230813','B004','E0002'),('M23010928','20230814','B005','E0002'),
('M23010929','20230910','B006','E0002'),('M23010930','20240105','B007','E0010'),
('M23010931','20240110','B008','E0010'),('M23010932','20240205','B009','E0010');



--Soal Pijat-pijat
--00 Tampilkan rekapitulasi

drop view DataGathering
create view DataGathering
as
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi], gd.[type] [Grade], 
'Message' as [type], mas.Tipe [detailType], em.Nama_Depan [Masanger], boma.Harga [Price], 6000 as [Service]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
union 
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi],gd.[type] [Grade],
'Goods' as [type], god.Tipe [detailType], '' as [Masanger],bogo.Harga [Price], 0 as [Service]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingGoods bogo on bo.ID_Booking=bogo.ID_Booking
join Goods god on god.ID_Goods=bogo.ID_Goods
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi

drop view Struk
create view Struk
as
select format([Date],'dd-MM-yy') [Dates],Reff,[BookNo],FORMAT(Tgl,'dd-MM-yy') Tanggal,MemberId,NamaCustomer,Alamat, Kota, Provinsi,Grade,
[type], detailType, Masanger,em.Nama_Depan as Kasir,Price,[Service],CAST(Price*0.11 as int) Tax,
cast(Price+[Service]+(Price*0.11) as int) Payment
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir

select*from DataGathering

--01 Tampilkan Masseus dengan pelanggan terbanyak
go
select m.Tipe Tipe
	from Massage m
	join BookingMessage bm on m.ID_Massage=bm.ID_Massage
	group by m.Tipe
having count(bm.ID_Booking_Message) = (
select max(Total)
from(
	select m.Tipe Tipe, count(bm.ID_Booking_Message) as Total
	from Massage m
	join BookingMessage bm on m.ID_Massage=bm.ID_Massage
	group by m.Tipe
)qty)

--02 Tampilkan Produk goods yang paling laris

go
select g.Tipe Tipe
from Goods g
join BookingGoods bg on g.ID_Goods=bg.ID_Goods
group by g.Tipe
having sum(bg.Quantity) = (
select max(Total)
from(
	select g.Tipe Tipe, sum(bg.Quantity) as Total
	from Goods g
	join BookingGoods bg on g.ID_Goods=bg.ID_Goods
	group by g.Tipe
)qty)

--03 Tampilkan service massage yang paling sedikit peminat

go
select m.Tipe Tipe
	from Massage m
	join BookingMessage bm on m.ID_Massage=bm.ID_Massage
	group by m.Tipe
having count(bm.ID_Booking_Message) = (
select min(Total)
from(
	select m.Tipe Tipe, count(bm.ID_Booking_Message) as Total
	from Massage m
	join BookingMessage bm on m.ID_Massage=bm.ID_Massage
	group by m.Tipe
)qty)

-- 04 Tampilkan jumlah service setiap bulannya.

select month(bo.Tanggal) Bulan, count(m.ID_Massage) Total
from Massage m
join BookingMessage bm on m.ID_Massage=bm.ID_Massage
join Booking bo on bo.ID_Booking=bm.ID_Booking
group by month(bo.Tanggal)

--05 Tampilkan tahun bulan penjualan terbanyak

select 
	year(bo.Tanggal) Tahun, month(bo.Tanggal) Bulan, sum(bg.Quantity) Total
from 
	Goods g
	join BookingGoods bg on g.ID_Goods=bg.ID_Goods
	join Booking bo on bo.ID_Booking=bg.ID_Booking
group by 
	year(bo.Tanggal), month(bo.Tanggal)
having 
	sum(bg.Quantity) = (
	select 
		max(qty.Total)
	from (
		select 
			year(bo.Tanggal) Tahun, month(bo.Tanggal) Bulan, sum(bg.Quantity) Total
		from 
			Goods g
			join BookingGoods bg on g.ID_Goods=bg.ID_Goods
			join Booking bo on bo.ID_Booking=bg.ID_Booking
		group by 
			year(bo.Tanggal), month(bo.Tanggal)
	)qty
)

--06 Tampilkan masing-masing total pendapatan service/massage & product/goods.

select 
	[type],detailType,sum(Payment) Total
from 
	Struk
group by 
	[type],detailType
order by
	[type]

select 'massage' Layanan, m.Tipe, sum(bm.Harga) Total
from Massage m
join BookingMessage bm on m.ID_Massage=bm.ID_Massage
group by m.Tipe
union
select 'goods' Layanan, g.Tipe, sum(bg.Quantity*bg.Harga) Total
from Goods g
join BookingGoods bg on g.ID_Goods=bg.ID_Goods
group by g.Tipe

--07 Dalam meningkatkan minat pelanggan, diadakan promo untuk para member dan mendapat potongan.
--		Bronze 2.5% untuk service tertentu
--		Silver 5% untuk service tertentu
--		Gold 5% untuk service & product tertentu
--		Platinum 8% untuk service & product tertentu
--	a. Tambahkan tabel dan/atau kolom diskon per grade dan product.
--	b. Tampilkan transaksi dengan diskon untuk customer sesuai grade-nya.

--drop view DataGatheringWithDiskon
create view DataGatheringWithDiskon
as
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], 
bo.Tanggal [Tgl], mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], 
kt.Nama [Kota], prov.Nama [Provinsi],gd.[type] [Grade], 'Message' as [type], 
mas.Tipe [detailType], em.Nama_Depan [Masanger], boma.Harga [Price], 6000 as [Service],
case when mem.ID_Grade='GD001' then 0.05 when mem.ID_Grade='GD002' then 0.05 when mem.ID_Grade='GD003' then 0.025
when mem.ID_Grade='GD004' then 0.08 end Promo
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
union 
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], 
bo.Tanggal [Tgl], mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], 
kt.Nama [Kota], prov.Nama [Provinsi],gd.[type] [Grade],'Goods' as [type], god.Tipe [detailType], 
'' as [Masanger],bogo.Harga [Price], 0 as [Service],case when mem.ID_Grade='GD001' then 0.05
when mem.ID_Grade='GD004' then 0.08 else 0 end Promo
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingGoods bogo on bo.ID_Booking=bogo.ID_Booking
join Goods god on god.ID_Goods=bogo.ID_Goods
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi

--drop view StrukWithDiskon
create view StrukWithDiskon
as
select format([Date],'dd-MM-yy') [Dates],Reff,[BookNo],FORMAT(Tgl,'dd-MM-yy') Tanggal,MemberId,NamaCustomer,Alamat, Kota, Provinsi,Grade,
[type], detailType, Masanger,em.Nama_Depan as Kasir,Price,CAST(Price*Promo as int) Diskon,[Service],CAST((Price-(Price*Promo))*0.11 as int) Tax,
cast((Price-(Price*Promo))+[Service]+((Price-(Price*Promo))*0.11) as money) Payment
from DataGatheringWithDiskon dg
join Employee em on em.ID_Employee=dg.Kasir

select*from StrukWithDiskon

--08 Tambahkan jenis kelamin untuk member/pelanggan & untuk Masseus, Jenis kelamin char(1) L/P

alter table Member add Jenis_kelamin char(1) CHECK (Jenis_kelamin='L' OR Jenis_kelamin='P')  
alter table Employee add Jenis_kelamin char(1) CHECK (Jenis_kelamin='L' OR Jenis_kelamin='P')  

--09 Tampilkan jumlah customer per Masseus dan bonus dimana Jumlah customer 5-10 mendapat 5% dari harga service, sedangkan >10 mendapat 8% dari harga serice.

create view RekapTransaksiMasseus
as
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [BookNo], 
bo.Tanggal [Tgl], mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], 
kt.Nama [Kota], prov.Nama [Provinsi],gd.[type] [Grade], 'Message' as [type], 
mas.Tipe [detailType], em.Nama_Depan [Masanger], boma.Harga [Price], 6000 as [Service],
case when mem.ID_Grade='GD001' then 0.05 when mem.ID_Grade='GD002' then 0.05 when mem.ID_Grade='GD003' then 0.025
when mem.ID_Grade='GD004' then 0.08 end Promo
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi

--drop view IncomeMasanger
create view IncomeMasanger
as
select *, qty.Total*6000 JumlahServis, case when qty.Total>=5 and qty.Total<=10 then 0.05 
when qty.Total>10 then 0.08 else 0 end BonusPercent, ((qty.Total*6000)*case when qty.Total>=5 and 
qty.Total<=10 then 0.05 when qty.Total>10 then 0.08 else 0 end) Bonus,
(qty.Total*6000)+((qty.Total*6000)*case when qty.Total>=5 and qty.Total<=10 then 0.05 
when qty.Total>10 then 0.08 else 0 end) Income
from(
select Masanger, count(*) as Total
from RekapTransaksiMasseus
group by Masanger
)qty

select Masanger, Total, Bonus, Income from IncomeMasanger

--10 Tampilkan tren jumlah customer harian dengan nama hari (Sunday-Saturday) segala waktu.

--drop view DayCek
create view DayCek as
select DATENAME(WEEKDAY, qty.DayOfWeek) Haris 
from
(
SELECT DATEADD(DAY, number, GETDATE()) AS DayOfWeek
FROM (
    SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) - 1 AS number
    FROM sys.columns
) AS days
WHERE number < 7
)qty

create view StrukDays as
select DATENAME(WEEKDAY, [Date]) Hari, count(*) Total
from Struk2
group by DATENAME(WEEKDAY, [Date])

select Haris as Hari, ISNULL(Total,0) Total
from DayCek
left join StrukDays on DayCek.Haris=StrukDays.Hari

select*from Struk2
create view Struk2
as
select [Date],Reff,[BookNo],Tgl,MemberId,NamaCustomer,Alamat, Kota, Provinsi,Grade,
[type], detailType, Masanger,em.Nama_Depan as Kasir,Price,CAST(Price*Promo as int) Diskon,[Service],CAST((Price-(Price*Promo))*0.11 as int) Tax,
cast((Price-(Price*Promo))+[Service]+((Price-(Price*Promo))*0.11) as money) Payment
from DataGatheringWithDiskon dg
join Employee em on em.ID_Employee=dg.Kasir










		







--select *
--from Invoice iv
--join Booking bo on iv.ID_Booking=bo.ID_Booking
--join Employee em on iv.Kasir=em.ID_Employee
--join Job_Title joti on joti.ID_Job=em.ID_Job

--join Member mem on mem.ID_Member=bo.ID_Member
--join Grade gd on gd.ID_Grade=mem.ID_Grade
--join Kota kt on kt.ID_Kota=mem.ID_Kota
--join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi