USE InventoryDb318
GO
--Group 01
--1. Tampilkan hasil penjualan Outlet Jaktim Jaya berdasarkan hari di tahun 2022
select 
	day(sel.SellingDate) Hari, pro.Harga*sel.Quantity HasilPenjualan
from Product pro
	join Selling sel on sel.KodeProduct=pro.Kode
	join Outlet ol on ol.Kode=sel.KodeOutlet
where ol.Nama='Jaktim Jaya' and year(sel.SellingDate)=2022
order by day(sel.SellingDate)
--2. Tampilkan nama produk & jumlah penjualan produk dari outlet Jakpus Jaya
select
	pro.Nama NamaProduk, sum(pro.Harga*sel.Quantity) JumlahPenjualan
from 
	Product pro
	join Selling sel on sel.KodeProduct=pro.Kode
	join Outlet ol on ol.Kode=sel.KodeOutlet
where
	ol.Nama='JakPus Jaya'
group by
	pro.Nama
--3. Tampilkan nama produk yang terjual di bulan Januari
select
	pro.Nama [Nama Produk]
from 
	Product pro
	join Selling sel on sel.KodeProduct=pro.Kode
where
	month(sel.SellingDate)= 1
--Group 03
--1. Tampilkan Kode Produk, Nama Produk dan  jumlah produk diatas rata-rata yang nama produknya berakhiran i dan mengubah Nama Produk menjadi huruf kapital
SELECT sel.KodeProduct, UPPER(pro.Nama), SUM(sel.Quantity) JumlahProduk
FROM Selling sel
JOIN Product pro ON sel.KodeProduct = pro.Kode
GROUP BY sel.KodeProduct, pro.Nama
HAVING SUM(sel.Quantity) > (SELECT AVG(sel.Quantity) FROM Selling sel) AND pro.Nama LIKE '%i'
--2. Tampilkan NamaProduk, Harga, dan JumlahTerjual, untuk produk yang belum pernah laku terjual
SELECT
pro.Nama, pro.Harga,
	CASE 
		WHEN SUM(sel.Quantity) IS NULL THEN '0'
	END JumlahTerjual
FROM Selling sel
RIGHT JOIN Product pro ON sel.KodeProduct = pro.Kode
GROUP BY  pro.Nama, pro.Harga
HAVING  SUM(sel.Quantity) IS NULL
--3. Tampilkan NamaKota dan JumlahTransaksi, untuk kota yang belum pernah terjadi transaksi
SELECT kt.Nama, COUNT(sel.Referensi) JumlahTransaksi
FROM Selling sel
JOIN Outlet ot ON sel.KodeOutlet = ot.Kode
RIGHT JOIN Kota kt ON ot.KodeKota = kt.Kode
GROUP BY kt.Nama
HAVING COUNT(sel.Referensi) = 0
---- Group 04
--1. tampilkan harga produk termurah perprovinsi
SELECT pmhPro.NamaProv, pmhPro.NamaProd, pmhPro.Harga 
FROM ProvMinHarga pmhPro
	JOIN 
(
		SELECT NamaProv, MIN(Harga) Harga
		FROM ProvMinHarga
		GROUP BY NamaProv
) 
pmh ON pmhPro.NamaProv = pmh.NamaProv AND pmhPro.Harga = pmh.Harga

ALTER VIEW ProvMinHarga
AS
SELECT Prov.Nama NamaProv,prod.Nama NamaProd,MIN(Harga) Harga
FROM Selling sell
JOIN Product prod on prod.Kode = sell.KodeProduct
JOIN Outlet  outl on outl.Kode = sell.KodeOutlet
JOIN Kota on Kota.Kode = outl.KodeKota
JOIN Provinsi prov on prov.Kode = Kota.KodeProvinsi
GROUP BY prov.Nama,prod.Nama
--2. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
SELECT outl.Kode,outl.Nama,prod.Nama,SUM(Quantity) Quantity,Harga,SUM((Harga*Quantity)) JumlahPenjualan
FROM Selling sell
JOIN Product prod on prod.Kode = sell.KodeProduct
JOIN Outlet  outl on outl.Kode = sell.KodeOutlet
WHERE prod.Nama IN ('Roti','Pasta gigi','Seblak') AND prod.Kode IN ('P0192','P9289','P9830')
GROUP BY outl.Kode,outl.Nama,prod.Nama,Harga
--3. Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT
Kota.Nama NamaKota,
	CASE 
		WHEN Kota.Nama = 'Jakarta Barat' THEN 'Jakbar Jaya'
		WHEN Kota.Nama = 'Jakarta Timur' THEN 'Jaktim Jaya'
		WHEN Kota.Nama = 'Jakarta Utara' THEN 'Jakut Jaya'
	END NamaOutlet
FROM Kota
LEFT JOIN Outlet outl ON Kota.Kode = outl.KodeKota
WHERE outl.Nama IS NULL AND Kota.Nama LIKE '%Jakarta%' 