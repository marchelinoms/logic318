	CREATE TABLE Agama
(
Id INT IDENTITY(1,1) NOT NULL,
Kode_Agama CHAR(5) PRIMARY KEY,
Deskripsi VARCHAR(20)
)
CREATE TABLE Mahasiswa
(
Id INT IDENTITY(1,1) NOT NULL,
Kode_Mahasiswa CHAR(5) PRIMARY KEY,
Nama_Mahasiswa VARCHAR(100) NOT NULL,
Alamat VARCHAR(200) NOT NULL,
Kode_Agama CHAR(5) NOT NULL,
Kode_Jurusan CHAR(5) NOT NULL
)
CREATE TABLE Ujian
(
Id INT IDENTITY(1,1) NOT NULL,
Kode_Ujian CHAR(5) PRIMARY KEY,
Nama_Ujian VARCHAR(50) NOT NULL,
Status_Ujian VARCHAR(100) NOT NULL
)
CREATE TABLE Type_Dosen
(
Id INT IDENTITY(1,1) NOT NULL,
Kode_TypeDosen CHAR(5) PRIMARY KEY,
Deskripsi VARCHAR(20)
)
CREATE TABLE Dosen
(
Id INT IDENTITY (1,1) NOT NULL,
Kode_Dosen CHAR(5) PRIMARY KEY,
Nama_Dosen VARCHAR(100),
Kode_Jurusan CHAR(5),
Kode_Type_Dosen CHAR(5)
)
CREATE TABLE Nilai
(
Id INT PRIMARY KEY IDENTITY(1,1),
Kode_Mahasiswa CHAR(5),
Kode_Ujian CHAR(5),
Nilai DECIMAL(8,2)
)


--- Practice Day 1----

--02.Buatlah query untuk mengubah column Nama_Dosen dengan type data VarChar dengan panjang 200 pada table Dosen
ALTER TABLE Dosen ALTER COLUMN Nama_Dosen VARCHAR(200)

--03.Buatlah query untuk menampilkan data berikut:
--Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan	Agama			
--M001	Budi Gunawan	Teknik Informatika	Islam
SELECT Kode_Mahasiswa,Nama_Mahasiswa,jur.Nama_Jurusan,agm.Deskripsi
FROM Mahasiswa as mhs
JOIN Agama as agm on agm.Kode_Agama = mhs.Kode_Agama
JOIN Jurusan as jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
WHERE Kode_Mahasiswa ='M001'

--04.Buatlah query untuk menampilkan data mahasiswa yang mengambil jurusan dengan Status Jurusan = Non Aktif
SELECT mhs.*,Nama_Jurusan,Status_Jurusan
FROM Mahasiswa as mhs
JOIN Jurusan as jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
WHERE jur.Status_Jurusan = 'Non Aktif'

--05.Buatlah query untuk menampilkan data mahasiswa dengan nilai diatas 80 untuk ujian dengan Status Ujian = Aktif
SELECT mhs.*,uji.Nama_Ujian,nil.Nilai
FROM Nilai as nil
JOIN Mahasiswa as mhs on mhs.Kode_Mahasiswa = nil.Kode_Mahasiswa
JOIN Ujian as uji on uji.Kode_Ujian = nil.Kode_Ujian
WHERE nil.Nilai > 80 AND uji.Status_Ujian = 'Aktif'

--06.Buatlah query untuk menampilkan data jurusan yang mengandung kata 'sistem'.
SELECT Nama_Jurusan
FROM Jurusan
WHERE Nama_Jurusan LIKE '%sistem%'

--07.Buatlah query untuk menampilkan mahasiswa yang mengambil ujian lebih dari 1.
SELECT mhs.Kode_Mahasiswa,mhs.Nama_Mahasiswa, COUNT(Nilai.Kode_Mahasiswa) as [Jumlah Ujian]
FROM Nilai
JOIN Mahasiswa mhs on Nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
JOIN Ujian uji on Nilai.Kode_Ujian = uji.Kode_Ujian
GROUP BY mhs.Kode_Mahasiswa,mhs.Nama_Mahasiswa
HAVING COUNT(Nilai.Kode_Mahasiswa) > 1

--08.Buatlah query untuk menampilkan data seperti berikut:
--Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan		Agama	Nama_Dosen					Status_Jurusan	Deskripsi
--M001				Budi Gunawan	Teknik Informatika	Islam	Prof. Dr. Retno Wahyuningsih	Aktif		Honorer
SELECT mhs.Kode_Mahasiswa,mhs.Nama_Mahasiswa,jur.Nama_Jurusan,agm.Deskripsi as [Agama],dos.Nama_Dosen,jur.Status_Jurusan,typedos.Deskripsi
FROM Mahasiswa mhs
JOIN Jurusan jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
JOIN Agama agm on agm.Kode_Agama = mhs.Kode_Agama
JOIN Dosen dos on dos.Kode_Jurusan = jur.Kode_Jurusan
JOIN Type_Dosen typedos on typedos.Kode_TypeDosen = dos.Kode_Type_Dosen
WHERE mhs.Kode_Mahasiswa = 'M001'

--09.Buatlah query untuk create view dengan menggunakan data pada no. 8, beserta query untuk mengeksekusi view tersebut.
GO
--This is how to create view
CREATE VIEW DataDosenPembimbing AS
SELECT mhs.Kode_Mahasiswa,mhs.Nama_Mahasiswa,jur.Nama_Jurusan,agm.Deskripsi as [Agama],dos.Nama_Dosen,jur.Status_Jurusan,typedos.Deskripsi
FROM Mahasiswa mhs
JOIN Jurusan jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
JOIN Agama agm on agm.Kode_Agama = mhs.Kode_Agama
JOIN Dosen dos on dos.Kode_Jurusan = jur.Kode_Jurusan
JOIN Type_Dosen typedos on typedos.Kode_TypeDosen = dos.Kode_Type_Dosen
WHERE mhs.Kode_Mahasiswa = 'M001'
GO
--This is how to call the view
SELECT *
FROM DataDosenPembimbing

--10.Buatlah query untuk menampilkan data mahasiswa beserta nilainya (mahasiswa yang tidak punya nilai juga ditampilkan).
SELECT mhs.*,Nilai
FROM Nilai nilai
RIGHT JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa

--11.Buatlah query untuk menampilkan data mahasiswa beserta nilainya yang memiliki nilai:
--A. Minimum
SELECT TOP 1 mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
ORDER BY Nilai

SELECT mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai = 
(
SELECT MIN(Nilai)
FROM Nilai
)
--B.Maximum
SELECT TOP 1 mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
ORDER BY Nilai DESC

SELECT mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai = 
(
SELECT MAX(Nilai)
FROM Nilai
)
--C.Diatas Rata-Rata
SELECT mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai > 
(
SELECT AVG(Nilai)
FROM Nilai
)
--D.Dibawah Rata-Rata
SELECT mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai < 
(
SELECT AVG(Nilai)
FROM Nilai
)
--12.Tambahkan kolom Biaya dengan type data Money/Number/Decimal 18,4 pada table Jurusan
ALTER TABLE Jurusan ADD Biaya DECIMAL(18,4)

--13.Tambahkan nilai kolom Biaya pada table Jurusan sebagai berikut:
--Nama_Jurusan							Biaya					
--Teknik Informatika	                1,500,000					
--Management Informatika	            1,550,000					
--Sistem Informasi						1,475,000					
--Sistem Komputer						1,350,000					
--Komputer Akuntansi	                1,535,000	


UPDATE Jurusan SET Biaya = 1500000 WHERE Kode_Jurusan ='J001'
UPDATE Jurusan SET Biaya = 1550000 WHERE Kode_Jurusan ='J002'
UPDATE Jurusan SET Biaya = 1475000 WHERE Kode_Jurusan ='J003'
UPDATE Jurusan SET Biaya = 1350000 WHERE Kode_Jurusan ='J004'
UPDATE Jurusan SET Biaya = 1535000 WHERE Kode_Jurusan ='J005'

--14.Buatlah query untuk menampilkan jurusan dengan biaya:
--A. Minimum
SELECT TOP 1 Jurusan.*
FROM Jurusan
ORDER BY Biaya

SELECT *
FROM Jurusan jur
WHERE Biaya = (SELECT MIN(Biaya) FROM Jurusan)
--B.Maximum
SELECT TOP 1 Jurusan.*
FROM Jurusan
ORDER BY Biaya DESC

SELECT *
FROM Jurusan jur
WHERE Biaya = (SELECT MAX(Biaya) FROM Jurusan)
--c. Diatas rata-rata
SELECT *
FROM Jurusan jur
WHERE Biaya > (SELECT AVG(Biaya) FROM Jurusan)
--d. Dibawah rata-rata
SELECT *
FROM Jurusan jur
WHERE Biaya < (SELECT AVG(Biaya) FROM Jurusan)


--Soal SQL Day 1 (Using InventoryDb)
--01.Tampilkan jumlah penjualan barang peroutlet per-tanggal
SELECT Outlet.Kode,sell.SellingDate, SUM(Quantity) as [Penjualan]
FROM Outlet
JOIN Selling sell on sell.KodeOutlet = Outlet.Kode
JOIN Product prod on prod.kode = sell.KodeProduct
GROUP BY Outlet.Kode,Outlet.Kode,sell.SellingDate
ORDER BY sell.SellingDate

--02 Tampilkan jumlah penjualan per tahun
SELECT YEAR(SellingDate) as [Tahun], SUM(Quantity) as [Jumlah Penjualan]
FROM Selling 
GROUP BY YEAR(SellingDate)

--03 Tapilkan jumlah product terlaris dan ter tidak laris per kota

SELECT t1.NamaKota,MAX(t1.Quantity) as [Terlaris],MIN(t1.Quantity) as [Tidak Terlaris]
FROM
(
	SELECT sell.KodeProduct as [KProd],SUM(Quantity) as [Quantity],kot.Nama as [NamaKota]
	FROM Selling sell
	JOIN Product prod on prod.Kode = sell.KodeProduct
	JOIN Outlet  outl on outl.Kode = sell.KodeOutlet
	JOIN Kota kot on kot.Kode = outl.KodeKota
	GROUP BY sell.KodeProduct,kot.Nama
)t1
GROUP BY t1.NamaKota

--04.Tampilkan jumlah penjualan per provinsi dan urutkan dari yang terbesar
	SELECT prov.Nama as [Provinsi],SUM(Quantity * Harga) as [Penjualan]
	FROM Selling sell
	JOIN Product prod on prod.Kode = sell.KodeProduct
	JOIN Outlet  outl on outl.Kode = sell.KodeOutlet
	JOIN Kota kot on kot.Kode = outl.KodeKota
	JOIN Provinsi prov on prov.Kode = kot.KodeProvinsi
	GROUP BY prov.Kode,prov.Nama
	ORDER BY Penjualan DESC

--05. Tampilkan referensi yang tidak sesuai dengan sellingdate
SELECT Referensi,YEAR(SellingDate) Tahun,MONTH(SellingDate) Bulan
FROM Selling sell
WHERE CAST(SUBSTRING(Referensi, 4,2)as int) <> YEAR(SellingDate) AND CAST(SUBSTRING(Referensi, 6,2)as int) <> MONTH(SellingDate)

--06. Tampilkan jumlah produk terjual pertahun peroutlet
SELECT KodeOutlet,YEAR(SellingDate) Tahun ,SUM(Quantity) Jumlah_Produk
FROM Selling sell
JOIN Product prod on sell.KodeProduct = prod.Kode
JOIN Outlet  outl on sell.KodeOutlet = outl.Kode
GROUP BY KodeOutlet,SellingDate
--07. Tampilkan jumlah penjualan peroutlet
SELECT KodeOutlet,SUM(Quantity*Harga) Jumlah_Penjualan
FROM Selling sell
JOIN Product prod on sell.KodeProduct = prod.Kode
JOIN Outlet  outl on sell.KodeOutlet = outl.Kode
GROUP BY KodeOutlet
--08. Tampilkan jumlah penjualan per bulan diurutkan berdasar bulan
SELECT MONTH(SellingDate) Bulan,SUM(Quantity*Harga) Jumlah_Penjualan
FROM Selling sell
JOIN Product prod on sell.KodeProduct = prod.Kode
GROUP BY MONTH(SellingDate)
--09. Tampilkan rata-rata jumlah penjualan setiap bulan
SELECT MONTH(SellingDate) Bulan,AVG(Quantity*Harga) Rata_Rata_Penjualan
FROM Selling sell
JOIN Product prod on sell.KodeProduct = prod.Kode
GROUP BY MONTH(SellingDate)
--10. Tampilkan produk dengan jumlah produk dibawah rata-rata
SELECT KodeProduct,SUM(Quantity) Jumlah_Produk
FROM Selling sell
JOIN Product prod on sell.KodeProduct = prod.Kode
GROUP BY KodeProduct,prod.Nama
HAVING SUM(Quantity)<
(
	SELECT AVG(Jumlah)Ratarata
	FROM 
	(
		SELECT SUM(Quantity)Jumlah
		FROM Selling
		GROUP BY KodeProduct
	)JumlahTiapProduk
)
--11.Tampilkan Jumlah Outlet Per Provinsi
SELECT Provinsi.Nama,COUNT(Outlet.Kode)JumlahOutlet
FROM Outlet
JOIN Kota on Kota.Kode = Outlet.KodeKota
JOIN Provinsi on Provinsi.Kode = Kota.KodeProvinsi
GROUP BY Provinsi.Nama
--12.Tampilkan Produk Terlaris Per Periode Bulanan
CREATE VIEW SelQty 
AS
SELECT YEAR(SellingDate) [Year], MONTH(SellingDate)[Month], KodeProduct,SUM(Quantity) Quantity
FROM Selling
GROUP BY YEAR(SellingDate),MONTH(SellingDate),KodeProduct

SELECT SelQty.*
FROM SelQty
JOIN 
(
SELECT [Year],[Month],MAX(Quantity) Quantity
FROM SelQty
GROUP BY [Year],[Month]
)SelMaxQty on SelMaxQty.Year = SelQty.Year AND SelQty.Month = SelMaxQty.Month AND SelMaxQty.Quantity = SelQty.Quantity
ORDER BY SelQty.Year,SelQty.Month

--13 Tampilkan provinsi yg menjual sampoo, quantity, harga & jumlah penjualan
select 
    prov.Nama,
	sum(sell.Quantity) Quantity,
	pro.Harga,
	sum(sell.Quantity * Harga) JumlahPenjualan
from Selling sell
	join Product pro
		on sell.KodeProduct = pro.Kode 
	join outlet ou
		on ou.Kode = sell.KodeOutlet
	join Kota ko
		on ou.KodeKota = ko.Kode
	join Provinsi prov
		on prov.Kode = ko.KodeProvinsi
where pro.Nama = 'Sampoo'
group by  prov.Nama, pro.Harga
--14 Tampilkan produk harga termahal setiap provinsi
SELECT pmhPro.NamaProv, pmhPro.NamaProd, pmhPro.Harga 
FROM ProvMaxHarga pmhPro
	JOIN 
(
		SELECT KodeProv, NamaProv, MAX(Harga) Harga
		FROM ProvMaxHarga
		GROUP BY KodeProv, NamaProv
) 
pmh ON pmhPro.KodeProv = pmh.KodeProv AND pmhPro.Harga = pmh.Harga
	
CREATE VIEW ProvMaxHarga
AS
SELECT prv.Kode KodeProv, prv.Nama NamaProv, 
	pro.Kode KodeProd, pro.Nama NamaProd, 
	MAX(Harga) Harga
FROM Selling sell
	JOIN Outlet Ou ON Ou.Kode = sell.KodeOutlet
	JOIN Product pro ON sell.KodeProduct = pro.Kode
	JOIN Kota kot ON Ou.KodeKota = kot.Kode
	JOIN Provinsi prv ON kot.KodeProvinsi = prv.Kode
GROUP BY prv.Kode, prv.Nama, pro.Kode, pro.Nama

--15 Tampilkan outlet dengan penjualan tertinggi & terendah
SELECT
		CASE
			WHEN sm.Penjualan = (SELECT * FROM max15) THEN 'Maksimal'
			WHEN sm.Penjualan = (SELECT * FROM min15) THEN 'Minimal'
		END Stat,
		sm.NamaOutlet NamaOutlet,
		sm.Penjualan
	FROM 
(		
	SELECT Outlet.Kode KodeOutlet, Outlet.Nama NamaOutlet,ISNULL(SUM(Quantity * Product.Harga), 0) Penjualan
		FROM Selling 
		RIGHT JOIN Outlet  ON Outlet.Kode = KodeOutlet
		RIGHT JOIN Product ON Product.Kode = KodeProduct
		GROUP BY Outlet.Kode,Outlet.Nama
) sm
	WHERE
		sm.Penjualan = (SELECT * FROM max15) OR
		sm.Penjualan = (SELECT * FROM min15)

ALTER VIEW max15 AS
(
SELECT MAX(Penjualan) Harga
FROM
(
	SELECT Outlet.Kode [KodeOutlet],Outlet.Nama [NamaOutlet],SUM(Quantity*Harga) Penjualan
	FROM Selling
		JOIN Product on Product.Kode = Selling.KodeProduct
		JOIN Outlet  on Outlet.Kode = Selling.KodeOutlet
	GROUP BY Outlet.Kode,Outlet.Nama
)t
)

ALTER VIEW min15 AS
(
SELECT MIN(Penjualan) Harga
FROM
(SELECT Outlet.Kode [KodeOutlet],Outlet.Nama [NamaOutlet],ISNULL(SUM(Quantity * Product.Harga), 0) Penjualan
	FROM Selling 
		RIGHT JOIN Product on Product.Kode = Selling.KodeProduct
		RIGHT JOIN Outlet  on Outlet.Kode = Selling.KodeOutlet
	GROUP BY Outlet.Kode,Outlet.Nama
)tMin
)


