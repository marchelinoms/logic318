﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_08
{
    public class FindTheMedian
    {
        public FindTheMedian()
        {
            Console.WriteLine("Masukkan barisan angka ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Sorting.Insertion(input);
            Printing.Array1Dim(input);
            Console.WriteLine($"Median = {FindMedian(input)}");
        }
        public double FindMedian(int[] input)
        {
            double median = input.Length % 2 == 0 ? (input[(input.Length / 2)] + input[(input.Length/2)-1])/2 : input[input.Length/2];   
            return median ;
        }
    }
}
