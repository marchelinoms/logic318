﻿//Soal 1
static void upahKaryawan()
{
    Console.WriteLine("---Upah Karyawan---");
    double upah = 0, lembur = 0,jamKerja = 0,golongan = 0,rate = 0;
    while(golongan < 1 || jamKerja < 1)
    {
        Console.Write("Golongan : ");
        golongan = int.Parse(Console.ReadLine());
        Console.Write("Jam Kerja: ");
        jamKerja = double.Parse(Console.ReadLine());
        if (golongan.GetType() != typeof(string) && jamKerja.GetType() != typeof(string))
            break;
        Console.WriteLine("Input Invalid!");
    }

    switch (golongan) 
    {
        case 1:
            rate = 2000;
            break;
        case 2:
            rate = 3000;
            break;
        case 3:
            rate = 4000 ;
            break;
        case 4:
            rate = 5000;
            break;
    }
    upah = jamKerja > 40 ? rate * 40 : rate * jamKerja;
    lembur = jamKerja > 40 ? 1.5 * rate * (Math.Round(jamKerja, MidpointRounding.ToPositiveInfinity) - 40) : 0;
    Console.WriteLine($"Upah    : {upah,9}");
    Console.WriteLine($"Lembur  : {lembur,9}");
    Console.WriteLine($"Total   : {upah + lembur,9}");
}

//Soal 2
static void splitString() 
{
    Console.WriteLine("---Split String---");
    string[] input;
    Console.WriteLine("Masukkan Kalimat : ");
    input = Console.ReadLine().ToLower().Split(" ");

    for(int i = 0; i < input.Length; i++)
        Console.WriteLine($"Kata{i+1} = {input[i]}");
    Console.WriteLine("Total kata adalah {0}", input.Length);
}

//Soal 3
static void manipulasiString()
{
    Console.WriteLine("---Manipulasi String---");
    string input;
    Console.Write("Masukkan Input : ");
    input = Console.ReadLine();
    string[] kata = input.Split(" ");
    char[] huruf;
    foreach(string s in kata)
    {
        huruf = s.ToCharArray();
        for (int i = 0; i < huruf.Length; i++) 
        {
            if (i == 0 || i == huruf.Length - 1)
            {
                if (i == huruf.Length - 1)
                    Console.Write(huruf[i] + " ");
                else
                    Console.Write(huruf[i]);
            }
            else
                Console.Write('*');
        }
    }
}
//Soal 4
static void manipulasiString2()
{
    Console.WriteLine("---Manipulasi String (2) ---");
    string input;
    Console.Write("Masukkan Input : ");
    input = Console.ReadLine();
    string[] kata = input.Split(" ");
    char[] huruf;
    foreach (string s in kata)
    {
        huruf = s.ToCharArray();
        for (int i = 0; i < huruf.Length; i++)
        {
            if (i == 0 || i == huruf.Length - 1)
            {
                if (i == huruf.Length - 1)
                    Console.Write("* ");
                else
                    Console.Write('*');
            }
            else
                Console.Write(huruf[i]);
        }
    }
}

//Soal 5
static void manipulasiString3()
{
    Console.WriteLine("---Manipulasi String (3) ---");
    string input;
    Console.Write("Masukkan Input : ");
    input = Console.ReadLine();
    string[] kata = input.Split(" ");
    char[] huruf;
    foreach (string s in kata)
    {
        huruf = s.ToCharArray();
        for (int i = 0; i < huruf.Length; i++)
        {
            if (i > 0) 
            {
                Console.Write(huruf[i]);
                if (i == huruf.Length - 1)
                    Console.Write(" ");
            }
        }
    }
}

//Soal 6
static void deretGeometri() 
{
    Console.WriteLine("---Deret Geometri---");
    int input = 0, batas = 0, rasio = 0;

    while (input < 1 || batas < 1 || rasio < 1) 
    {
        Console.Write("Masukkan angka basis : ");
        input = int.Parse(Console.ReadLine());
        Console.Write("Sampai Suku ke - : ");
        batas = int.Parse(Console.ReadLine());
        Console.Write("Masukkan nilai beda/rasio : ");
        rasio = int.Parse(Console.ReadLine());
        if (input.GetType() == typeof(int) && batas.GetType() == typeof(int) && rasio.GetType() == typeof(int))
            break;

        Console.WriteLine("Input Invalid");
    }
    Console.Write(input + " ");
    for(int i = 1;i < batas;i++)
    {
        input *= rasio;
        if (i % 2 == 0)
        {
            Console.Write(input + " ");
        }
        else
        {
            Console.Write("* ");
        }
    }
}

//Soal 7
static void fibonacci()
{
    Console.WriteLine("---Fibonacci Sequence---");
    int a = 0, b = 1, c,n = 0;

    while (n < 1)
    {
        Console.Write("Masukkan nilai n : ");
        n = int.Parse(Console.ReadLine());

        if (n.GetType() == typeof(int))
            break;

        Console.WriteLine("Input Invalid");
    }

    Console.Write(b + " ");
    for (int i = 1; i < n; ++i)
    {
        c = a + b;
        Console.Write(c + ",");
        a = b;
        b = c;
    }
}

//Soal 8
static void array2D()
{
    Console.WriteLine("2D Array");

    int baris = 0, kolom = 0, n = 0;
    while (n < 1)
    {
        Console.Write(" n = ");
        n = int.Parse(Console.ReadLine());
        if (n.GetType() == typeof(int))
            break;

        Console.WriteLine("Input Invalid");
    }

    for (baris = 1; baris <= n; baris++)
    {
        for (kolom = 1; kolom <= n; kolom++)
        {
            if (baris == 1 || baris == n)
            {
                if (baris == n)
                {
                    Console.Write(n - kolom + 1 + "\t");
                }
                else
                {
                    Console.Write(kolom + "\t");
                }
            }
            else
            {
                if (kolom == 1 || kolom == n)
                    Console.Write("*\t");
                else
                    Console.Write(" \t");
            }
        }
        Console.WriteLine();
    }
}
