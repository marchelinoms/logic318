﻿
static void Break()
{
    for(int i = 0; i < 10; i++)
    {
        if (i == 5)
            break;
        Console.WriteLine(i);
    }
}

static void Continue()
{
    for(int i = 0; i < 10; i++)
    {
        if (i == 7)
            continue;
        Console.WriteLine(i);
    }
}

static void stringToCharArray()
{
    Console.WriteLine("---String To Char Array---");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    char[] array = kalimat.ToCharArray();
    for(int i = 0; i < array.Length; i++)
        Console.WriteLine(array[i]);
}

static void indexOf()
{
    Console.Write("Masukkan item (value) : ");
    int item = int.Parse(Console.ReadLine());

    List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6 };
    int[] array = new int[] {5,6,7,8,9,10};

    int indexList = list.IndexOf(item);
    int indexArray = Array.IndexOf(array, item);

    if (indexList != -1)
        Console.WriteLine("List Element {0} is found at index {1}", item, indexList);
    else
        Console.WriteLine("Element not found in the given list");

    if (indexArray != -1)
        Console.WriteLine("List Element {0} is found at index {1}", item, indexArray);
    else
        Console.WriteLine("Element not found in the given Array");
}

static void datetime()
{
    DateTime dt1 = new DateTime();
    DateTime dt2 = DateTime.Now;
    DateTime dt3 = new DateTime(2023, 5, 4);
    DateTime dt4 = new DateTime(2023,5,4,11,55,55);

    Console.WriteLine(dt1);
    Console.WriteLine(dt2);
    Console.WriteLine(dt3);
    Console.WriteLine(dt4);
}

static void datetimeParsing()
{
    Console.WriteLine("---DateTime Parsing---");
    Console.Write("Masukkan Tanggal : ");
    string dateString = Console.ReadLine();

    try 
    {
        DateTime date = DateTime.ParseExact(dateString,"dd/MM/yyyy",null);
        Console.WriteLine(date);
    }
    catch(Exception e)
    {
        Console.WriteLine("Format yang anda masukkan salah!");
        Console.WriteLine(e.ToString());
    }
}

static void datetimeProperties()
{
    Console.WriteLine("---DateTime Properties---");
    DateTime date = new DateTime(2023, 5, 4, 11, 1, 45);

    int tahun = date.Year;
    int bulan = date.Month;
    int tanggal = date.Day;
    int jam = date.Hour;
    int menit = date.Minute;
    int detik  = date.Second;
    int dayOfWeek = (int)date.DayOfWeek;
    string hari = date.DayOfWeek.ToString("dddd");
    string hari2 = date.DayOfWeek.ToString();


    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"Tanggal : {tanggal}");
    Console.WriteLine($"Jam : {jam}");
    Console.WriteLine($"Menit : {menit}");
    Console.WriteLine($"Detik : {detik}");
    Console.WriteLine($"DayOfWeek : {dayOfWeek}");
    Console.WriteLine($"Hari : {hari}");
    Console.WriteLine($"Hari2 : {hari2}");
}

static void timespan()
{
    DateTime date1 = new DateTime(2023, 5, 4, 11, 35, 45);
    DateTime date2 = new DateTime(2023, 6, 14, 12, 45, 55);
    TimeSpan interval = date2 - date1;

    Console.WriteLine($"No. Of Days: {interval.Days}");
    Console.WriteLine($"Total No. Of Days: {interval.TotalDays}");
    Console.WriteLine($"No. Of Hours: {interval.Hours}");
    Console.WriteLine($"Total No. Of Hours: {interval.TotalHours}");
    Console.WriteLine($"No. Of Minutes: {interval.Minutes}");
    Console.WriteLine($"Total No. Of Minutes: {interval.TotalMinutes}");
    Console.WriteLine($"No. Of Seconds: {interval.Seconds}");
    Console.WriteLine($"Total No. Of Seconds: {interval.TotalSeconds}");
    Console.WriteLine($"No. Of MiliSeconds: {interval.Milliseconds}");
    Console.WriteLine($"Total No. Of MiliSeconds: {interval.TotalMilliseconds}");
    Console.WriteLine($"Ticks: {interval.Ticks}");
}