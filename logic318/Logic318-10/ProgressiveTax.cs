﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_10
{
    public class ProgressiveTax
    {
        public ProgressiveTax()
        {
            Console.WriteLine("Masukkan penghasilan tahunan : ");
            double penghasilan = int.Parse(Console.ReadLine());
            Console.WriteLine(IncomeTax(penghasilan));
        }
        public double IncomeTax(double penghasilan)
        {
            double pkp = penghasilan - 25000;
            double pajak = 0;
            if (pkp < 1)
                return 0;
            else
            {
                if (pkp - 50000 > 0)
                    pajak += 50000 * 0.05;
                else
                    pajak += pkp * 0.05;

                if (pkp > 50000)
                {
                    if (pkp - 50000 > 50000)
                        pajak += 50000 * 0.1;
                    else
                        pajak += (pkp - 50000) * 0.1;
                }
                if (pkp > 100000)
                {
                    if (pkp - 100000 > 100000)
                        pajak += 100000 * 0.15;
                    else
                        pajak += (pkp - 100000) * 0.15;
                }
                if (pkp > 200000)
                    pajak += (pkp - 200000) * 0.25;
            }

            return pajak;
        }
    }
}
