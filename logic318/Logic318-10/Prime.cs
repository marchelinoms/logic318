﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_10
{
    public class Prime
    {
        public Prime()
        {
            int n = int.Parse(Console.ReadLine());

            for (int i = 2; i < n + 2; i++)
            {
                if (PrimeNumbers.CheckPrime(i)) 
                    Console.Write($"{i}\t");
                else n++;
            }
        }
    }
}
