﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_10
{
    public class SimplifiedChessEngine
    {
        public SimplifiedChessEngine()
        {
            string qw = "", qb = "";
            while(qw == qb)
            {
                Console.WriteLine("Queen White : ");
                qw = Console.ReadLine().ToUpper();
                Console.WriteLine("Queen Black : ");
                qb = Console.ReadLine().ToUpper();
            }

            string[,] cBoard = new string[4, 4];
            string rowMap = "4321";
            string colMap = "ABCD";

            
            for (int i = 0; i < cBoard.GetLength(0); i++)
            {
                for (int j = 0; j < cBoard.GetLength(1); j++)
                {
                    if (i == rowMap.IndexOf(qw[1]) || j == colMap.IndexOf(qw[0])
                        || rowMap.IndexOf(qw[1]) + j == colMap.IndexOf(qw[0]) + i
                        || rowMap.IndexOf(qw[1]) + colMap.IndexOf(qw[0]) == i + j)
                        cBoard[i, j] = "x";
                    else cBoard[i, j] = ".";
                }
            }


            Console.WriteLine(cBoard[rowMap.IndexOf(qb[1]), colMap.IndexOf(qb[0])] == "x" ? "Yes" : "No");

            cBoard[rowMap.IndexOf(qw[1]), colMap.IndexOf(qw[0])] = "Qw";
         
            cBoard[rowMap.IndexOf(qb[1]), colMap.IndexOf(qb[0])] = "Qb";

            Printing.Array2Dim(cBoard);

        }
    }
}
