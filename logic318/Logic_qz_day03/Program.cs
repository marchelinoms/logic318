﻿Menu();

static void Menu()
{
    Console.WriteLine("---Logic Quiz Day 2----\n");
    Console.WriteLine("---1.Grade Mahasiswa---");
    Console.WriteLine("---2.Isi Pulsa---");
    Console.WriteLine("---3.Grab Food---");
    Console.WriteLine("---4.Shopee---");
    Console.WriteLine("---5.Generasi---");
    Console.WriteLine("---6.Slip Gaji---");
    Console.WriteLine("---7.Hitung BMI---");
    Console.WriteLine("---8.Nilai Rata-Rata---");
    Console.WriteLine("---0.Exit---");

    Console.Write("Masukkan Pilihan Anda : ");
    string pilihan = Console.ReadLine()!;

    bool ulang;
    switch (pilihan)
    {
        case "1":
            while (true)
            {
                gradeMahasiswa();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break;
        case "2":
            while (true)
            {
                isiPulsa();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break;
        case "3":
            while (true)
            {
                grabFood();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break;
        case "4":
            while (true)
            {
                shopee();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break;
        case "5":
            while (true)
            {
                generasi();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break;
        case "6":
            while (true)
            {
                slipGaji();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break; 
        case "7":
            while (true)
            {
                BMI();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break; 
        case "8":
            while (true)
            {
                nilaiRataRata();
                ulang = ulangi();
                if (ulang == false)
                    break;
                else
                    Menu();
            }
            break;
        case "0":
            while (true)
            {
                Console.Write("Sure want to exit? (y/n) :");
                pilihan = Console.ReadLine();
                if (pilihan == "y" || pilihan == "n") 
                { 
                    break;
                }
                else
                    Console.WriteLine("Input Invalid");
            }
            if (pilihan == "y")
                Environment.Exit(0);
            else
                Menu();
            break;
        default:
            Console.Clear();
            Console.WriteLine("Input Invalid");
            Menu();
            break;
    }
}
static bool ulangi()
{
    while (true)
    {
        Console.Write("Ulangi Proses ? (y/n) : ");
        string pilihan = Console.ReadLine()!;
        if (pilihan != "y" && pilihan != "n")
            Console.WriteLine("Input Invalid");
        else
        {
            if (pilihan == "n")
                return false;
            else
                return true;
            break;
        }
    }
}

//Soal 1
static void gradeMahasiswa()
{
    Console.WriteLine("---Grade Mahasiswa---");
    int nilai = 0;
    string grade = "";
    while (true)
    {
        Console.Write("Masukkan Nilai : ");
        nilai = int.Parse(Console.ReadLine());
        if (nilai < 0 && nilai > 100)
            Console.WriteLine("Input Invalid");
        else
            break;
    }
    if (nilai >= 90)
        grade = "A";
    else if (nilai < 90 && nilai >= 70)
        grade = "B";
    else if (nilai < 70 && nilai >= 50)
        grade = "C";
    else
        grade = "E";
    Console.WriteLine($"Grade = {grade}");
}

//Soal 2
static void isiPulsa()
{
    Console.WriteLine("---Isi Pulsa---");
    int pulsa = 0, point = 0;
        while (true)
        {
            Console.Write("Berapa Pulsa yang hendak diisikan? : ");
            pulsa = int.Parse(Console.ReadLine());

            if (pulsa < 0)
                Console.WriteLine("Input Invalid");
            else
                break;
        }
    if (pulsa >= 100000)
        point += 800;
    else if (pulsa >= 50000 && pulsa < 100000)
        point += 400;
    else if (pulsa >= 25000 && pulsa < 50000)
        point += 200;
    else
        point += 80;

    Console.WriteLine($"Pulsa : {pulsa}\nPoint : {point}");
}

//Soal 3
static void grabFood()
{
    Console.WriteLine("---Grab Food---");
    double belanja = 0, jarak = 0, ongkir = 0, total = 0,diskon = 0;
    string promo = "";

    while (true) 
    {
        Console.Write("Belanja   : ");
        belanja = double.Parse(Console.ReadLine());
        Console.Write("Jarak(km) : ");
        jarak = double.Parse(Console.ReadLine().Replace('.',','));
        if (belanja < 0)
            Console.WriteLine("Input Invalid, Periksa Kembali Inputan Belanja Anda");
        else if (jarak < 0)
            Console.WriteLine("Input Invalid, Periksa Kembali Inputan Jarak Anda");
        else
            break;
    }
    Console.Write("Masukkan Promo : ");
    promo = Console.ReadLine().ToUpper();

    ongkir = jarak <= 5 ? 5000 : 5000+(1000*(Math.Round(jarak-5,MidpointRounding.ToPositiveInfinity)));
    bool minBelanja = belanja >= 30000 ? true : false;

    if (minBelanja == true && promo == "JKTOVO") 
        diskon = belanja * 0.4;
    if (diskon > 30000)
        diskon = 30000;

    total = belanja + ongkir - diskon;

    Console.WriteLine($"Belanja       : {Math.Round(belanja)}");
    Console.WriteLine($"Diskon 40%    : {Math.Round(diskon)}");
    Console.WriteLine($"Ongkir        : {Math.Round(ongkir)}");
    Console.WriteLine($"Total Belanja : {Math.Round(total)}");
}

//Soal 4
static void shopee()
{
    Console.WriteLine("---Shopee---");
    int belanja = 0, ongkir = 0, kodeVoucher = 0, diskonOngkir = 0, diskonBelanja = 0, total = 0;

    while (true)
    {
        Console.Write("Belanja : ");
        belanja = int.Parse(Console.ReadLine());
        Console.Write("Ongkos Kirim : ");
        ongkir = int.Parse(Console.ReadLine());
        if (belanja < 0 || ongkir < 0)
            Console.WriteLine("Input Invalid");
        else
            break;
    }

    Console.WriteLine("Pilihan Voucher\n1. Min Order 30rb free ongkir 5rb dan potongan harga belanja 5rb");
    Console.WriteLine("Pilihan Voucher\n2. Min Order 50rb free ongkir 10rb dan potongan harga belanja 10rb");
    Console.WriteLine("Pilihan Voucher\n3. Min Order 100rb free ongkir 20rb dan potongan harga belanja 10rb");

    while (true) { 
    Console.Write("Pilih Voucher : ");
    kodeVoucher = int.Parse(Console.ReadLine());
    bool checkValid = true;
    switch (kodeVoucher)
    {
        case 1:
            checkValid = belanja >= 30000 ? true : false;
            if (checkValid == true)
            {
                diskonBelanja = 5000;
                diskonOngkir = 5000;
            }
            else
            {
                Console.WriteLine("Minimum Belanja 30 ribu, anda belum dapat menggunakan voucher ini");
            }
            break;
        case 2:
            checkValid = belanja >= 50000 ? true : false;
            if (checkValid == true)
            {
                diskonBelanja = 10000;
                diskonOngkir = 10000;
            }
            else
            {
                Console.WriteLine("Minimum Belanja 50 ribu, anda belum dapat menggunakan voucher ini");
            }
            break;
        case 3:
            checkValid = belanja >= 100000 ? true : false;
            if (checkValid == true)
            {
                diskonBelanja = 10000;
                diskonOngkir = 20000;
            }
            else
            {
                Console.WriteLine("Minimum Belanja 100 ribu, anda belum dapat menggunakan voucher ini");
            }
            break;
        default:
            Console.WriteLine("Input Invalid");
            break;
    }
        if (kodeVoucher > 0  && kodeVoucher < 4 && checkValid == true)
            break;
}
    total = belanja + ongkir - (diskonBelanja + diskonOngkir);
    Console.WriteLine($"Belanja : {belanja}");
    Console.WriteLine($"Ongkos Kirim : {ongkir}");
    Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
    Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
    Console.WriteLine($"Total Belanja : {total}");
}

//Soal 5
static void generasi()
{
    Console.WriteLine("---Generasi---");
    int tahunLahir = 0;
    string nama = "", generasi = "";
    Console.Write("Masukkan nama anda: ");
    nama = Console.ReadLine();
    while (true) 
    {
        Console.Write("Tahun berapa anda lahir? : ");
        tahunLahir = int.Parse(Console.ReadLine());
        if (tahunLahir < 0)
            Console.WriteLine("Input Invalid");
        else
            break;
    }
    if (tahunLahir >= 1995 && tahunLahir < 2016)
        generasi = "Generasi Z";
    else if (tahunLahir >= 1980 && tahunLahir < 1995)
        generasi = "Generasi Y";
    else if (tahunLahir >= 1965 && tahunLahir < 1980)
        generasi = "Generasi X";
    else if (tahunLahir >= 1944 && tahunLahir < 1965)
        generasi = "Generasi Baby Boomer";
    else
        Console.WriteLine("Generasi anda belum memiliki istilah tersendiri");

    Console.WriteLine("{0}, berdasarkan tahun lahir anda tergolong {1}", nama, generasi);    
}

//Soal 6
static void slipGaji() 
{
    Console.WriteLine("---Slip Gaji---");
    Decimal gapok = 0, tunjangan = 0, banyakBulan = 0, pajak = 0, persen = 0,bpjs = 0.03M,total = 0;
    string nama = "";
    Console.Write("Nama\t\t:");
    nama = Console.ReadLine();

    while (true) 
    {
        Console.Write("Tunjangan\t:");
        tunjangan = int.Parse(Console.ReadLine());
        Console.Write("Gapok\t\t:");
        gapok = int.Parse(Console.ReadLine());
        if (gapok < 0 || tunjangan < 0)
            Console.WriteLine("Input Invalid");
        else
            break;
    }
    while (true)
    {
        Console.Write("BanyakBulan\t:");
        banyakBulan = int.Parse(Console.ReadLine());
        if (banyakBulan < 0)
            Console.WriteLine("Input Invalid");
        else
            break;
    }
    if ((gapok + tunjangan) > 10000000)
        persen = 0.15M;
    else if ((gapok + tunjangan) >= 5000000 && (gapok + tunjangan) <= 10000000)
        persen = 0.1M;
    else
        persen = 0.05M;

    pajak = persen * (gapok + tunjangan);
    bpjs  *= (gapok + tunjangan);
    total = (gapok + tunjangan) - (pajak + bpjs); 
    Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut :");
    Console.Write("Pajak\t\t{0}\nBPJS\t\t{1}\nGaji/bln\t{2}\nTotalGaji\t{3}", pajak.ToString("Rp #,##"), bpjs.ToString("Rp #,##"), total.ToString("Rp #,##"), (total * banyakBulan).ToString("Rp #,##"));
}

//Soal 7
static void BMI() 
{
    Console.WriteLine("---Hitung BMI---");
    double berat = 0, tinggi = 0, BMI = 0;
    string hasil = "";
    while (true) 
    {    
        Console.Write("Masukkan berat badan anda  (kg) : ");
        berat = double.Parse(Console.ReadLine());
        Console.Write("Masukkan tinggi badan anda (cm) : ");
        tinggi = double.Parse(Console.ReadLine())/100;
        if (berat < 0 || tinggi < 0)
            Console.WriteLine("Input Invalid");
        else
            break;
    }
    BMI = berat / Math.Pow(tinggi, 2);
    if (BMI > 25)
        hasil = "gemuk";
    else if (BMI < 25 && BMI >= 18.5)
        hasil = "langsing";
    else
        hasil = "terlalu kurus";
    Console.WriteLine($"Nilai BMI anda : {Math.Round(BMI,4)}");
    Console.WriteLine($"Anda termasuk berbadan {hasil}");
}

//Soal 8
static void nilaiRataRata()
{
    Console.WriteLine("---Nilai Rata-Rata---");
    int mtk = 0, fisika = 0, kimia = 0, ratarata = 0;

    while (true)
    {
        Console.Write("Masukkan Nilai MTK : ");
        mtk = int.Parse(Console.ReadLine());
        Console.Write("Masukkan Nilai Fisika : ");
        fisika = int.Parse(Console.ReadLine());
        Console.Write("Masukkan Nilai Kimia : ");
        kimia = int.Parse(Console.ReadLine());
        if (mtk <= 0 && fisika <= 0 && kimia <= 0)
            if (mtk > 0 && fisika > 0 && kimia > 0)
                break;
            else
                Console.WriteLine("Input Invalid");
    }

    ratarata = (mtk+fisika+kimia)/3;
    string hasil = ratarata < 50 ? "Maaf\nKamu Gagal" : "Selamat\nKamu Berhasil\nKamu Hebat";

    Console.WriteLine($"Nilai Rata-Rata : {ratarata} ");
    Console.WriteLine(hasil);
}