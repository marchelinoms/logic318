﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public static class Printing
    {
        public static void Array1Dim(string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]}\t");
            }
        }
        public static void Array1Dim(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]}\t");
            }
        }
        public static void Array2Dim(string[,] arr)
        {
            //Row = Baris
            //Col = Kolom

            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for (int col = 0; col < arr.GetLength(1); col++)
                {
                    Console.Write($"{arr[row, col]}\t");        
                }
                Console.WriteLine();
            }
        }
        public static void Array2Dim(int[,] arr)
        {
            //Row = Baris
            //Col = Kolom

            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for (int col = 0; col < arr.GetLength(1); col++)
                {
                    Console.Write($"{arr[row, col]}\t");        
                }
                Console.WriteLine();
            }
        }
    }
}
