﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public static class Sorting
    {
        public static int[] Insertion(int[] arr)
        {
            for(int i = 1; i < arr.Length; i++)
            {
                for(int j = i; j > 0; j--)
                {
                    if (arr[j] < arr[j-1])
                    {
                        var temp = arr[j - 1];
                        arr[j-1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
    }
}
