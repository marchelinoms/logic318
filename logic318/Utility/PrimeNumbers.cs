﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class PrimeNumbers
    {
        public static bool CheckPrime(int input)
        {
            for (int i = 2; i < Math.Floor(Math.Sqrt(input)) + 1; i++)
                if (input % i == 0) return false;
            return true;
        }
    }
}
