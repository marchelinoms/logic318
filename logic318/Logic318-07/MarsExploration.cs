﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class MarsExploration
    {
        public MarsExploration()
        {
            string input = "";
            while (input.Length % 3 != 0 || input.Length < 1)
            {
                Console.Write("Input message : ");
                input = Console.ReadLine().ToUpper();
            }
            Console.WriteLine(input);
            Console.WriteLine(faultSignal(input));
        }
        public static int faultSignal(string input)
        {
            int count = 0;
            for (int i = 0; i < input.Length; i+=3)
            {
                if (input[i] != 'S' || input[i+1] != 'O' || input[i+2] != 'S' )
                    count++;
                else
                    count = count > 0 ? count-- : 0;
            }
            return count;
        }
    }
}
