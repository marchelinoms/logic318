﻿namespace Logic318_07;

public class Program
{
    public Program()
    {
        Menu();
    }
    static void Main(string[] args)
    {
        Menu();
    }
    static void Menu()
    {
        int soal = 0;
        Console.WriteLine("---Day 07---");
        while (soal < 1)
        {
            Console.WriteLine("---1. Camel Case---");
            Console.WriteLine("---2. Strong Password---");
            Console.WriteLine("---3. Caesar Cipher---");
            Console.WriteLine("---4. Mars Exploration---");
            Console.WriteLine("---5. HackerRank in a String!---");
            Console.WriteLine("---6. Pangram---");
            Console.WriteLine("---7. Separate The Numbers---");
            Console.WriteLine("---8. Gemstones---");
            Console.WriteLine("---9. Making Anagram---");
            Console.WriteLine("---10. Two Strings---");
            Console.WriteLine("Masukkan no soal: ");
            soal = int.Parse(Console.ReadLine());
        }
        switch (soal)
        {
            case 1:
                CamelCase camelCase = new CamelCase();
                break;
            case 2:
                StrongPassword strongPassword = new StrongPassword();
                break;
            case 3:
                CaesarCipher caesarCipher = new CaesarCipher();
                break;
            case 4:
                MarsExploration marsExploration = new MarsExploration();
                break;
            case 5:
                HackerRankInaString hackerRankInaString = new HackerRankInaString();
                break;
            case 6:
                Pangrams pangrams = new Pangrams();
                break;
            case 7:
                SeparateTheNumbers separateTheNumbers = new SeparateTheNumbers();
                break;
            case 8:
                Gemstones gemstones = new Gemstones();
                break;
            case 9:
                Anagrams anagrams = new Anagrams();
                break;
            case 10:
                TwoStrings twoStrings = new TwoStrings();
                break;
        }

    }
}