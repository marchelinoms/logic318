﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class CaesarCipher
    {
        public CaesarCipher()
        {
            Console.Write("Input Message : ");
            string input = Console.ReadLine();
            Console.Write("Rotate : ");
            int rotate = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil : {Cipher(input,rotate)}");
        }
        public static string Cipher(string input, int rotate)
        {
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            string result = "";
            foreach (char c in input)
            {
                string cLower = c.ToString().ToLower();
                if (!alphabet.Contains(cLower))
                    result = $"{result}{c}";
                else
                {
                    int index = alphabet.IndexOf(cLower);
                    string shiftedChar = alphabet[(index + rotate) % alphabet.Length].ToString();
                    if (!cLower.Equals(c.ToString()))
                        shiftedChar = shiftedChar.ToUpper();
                    result = $"{result}{shiftedChar}";
                }
            }

            return result;
        }
    }
}
