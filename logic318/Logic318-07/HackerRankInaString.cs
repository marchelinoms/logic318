﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class HackerRankInaString
    {
        public HackerRankInaString()
        {
            Console.WriteLine("Masukkan String : ");
            string input = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine("Masukkan String yang dicari : ");
            string check = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine(CheckHackerRank(input,check));
        }
        public static string CheckHackerRank(string input,string check)
        {
            string result = "";
            int index = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (index < check.Length && input[i] == check[index])
                    index++;
            }
            result = index == check.Length ? "YES" : "NO";
            return result;
        }
    }
}
