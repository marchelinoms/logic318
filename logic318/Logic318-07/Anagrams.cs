﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class Anagrams
    {
        public Anagrams()
        {
            string input, input2;
            Console.WriteLine("Masukkan kata/kalimat : ");
            input = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine("Masukkan kata/kalimat : ");
            input2 = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine(CheckAnagram(input,input2));
        }

        public static int CheckAnagram(string input, string input2)
        {
            int del = 0;
            List<Char> s = input.ToList();
            List<Char> s2 = input2.ToList();
            foreach (char c in s) 
            {
                if (s2.Contains(c))
                {
                    s2.Remove(c);
                    del++;
                }
            }
            
            return (s.Count - del) + (s2.Count);
        }
    }
}
