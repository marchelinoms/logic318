﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class Gemstones
    {
        public Gemstones()
        {
            int batu = 0;
            Console.Write("Berapa banyak batu : ");
            batu = int.Parse(Console.ReadLine());
            string[] arrmineral = new string[batu];

            for(int i = 0; i < batu; i++)
            {
                Console.Write($"Masukkan batu {i+1} : ");
                arrmineral[i] = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            }

            Console.WriteLine(GemCounter(arrmineral));
        }
        public static int GemCounter(string[] minerals)
        {
            int count = 0, gems = 0;

            for (char c = 'a'; c <= 'z'; c++)
            {
                for (int j = 0; j < minerals.Length; j++)
                {
                    if (minerals[j].Contains(c))
                        count++;
                }

                if (count == minerals.Length)
                    gems++;
            }

            return gems;
        }
    }
}
