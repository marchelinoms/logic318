﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class StrongPassword
    {
        public StrongPassword()
        {
            Console.WriteLine("Masukkan Password : ");
            string input = Console.ReadLine();
            Console.WriteLine(input);
            Console.WriteLine(strongChecker(input));
        }
        public int strongChecker(string input)
        {
            string libSpecialChar = "!@#$%^&*()-+";
            string numbers = "0123456789";
            string lower_case = "abcdefghijklmnopqrstuvwxyz";
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int count = 0;

            if (input.IndexOfAny(numbers.ToCharArray()) == -1) count++;
            if (input.IndexOfAny(lower_case.ToCharArray()) == -1) count++;
            if (input.IndexOfAny(upper_case.ToCharArray()) == -1) count++;
            if (input.IndexOfAny(libSpecialChar.ToCharArray()) == -1) count++;

            if(input.Length < 6)
                count = count > 6 - input.Length ? count : 6 - input.Length;

            return count;
        }
    }
}
