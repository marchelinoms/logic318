﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class Pangrams
    {
        public Pangrams()
        {
            Console.WriteLine("Masukkan kalimat : ");
            string input = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine(CheckPangram(input) == true ? "Pangram" : "Not Pangram");
        }
        public static bool CheckPangram(string input)
        {
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            bool result;
            List<char> chars = new List<char>();
            for(int i = 0; i < input.Length; i ++) 
            {
                for(int j = 0; j < alphabet.Length; j++)
                {
                    if (input.ToLower()[i] == alphabet[j])
                        chars.Add(input.ToLower()[i]);
                }
            }
            var distinct = chars.Distinct();
            return result = distinct.Count() == 26 ? true : false;
        }
    }
}
