﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class TwoStrings
    {
        public TwoStrings()
        {
            Console.Write("Jumlah Kata : ");
            int kata = int.Parse(Console.ReadLine());
            List<bool> list = new List<bool>();
            while(kata > 0)
            {
                Console.WriteLine("Masukkan kalimat/kata : ");
                string input = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
                Console.WriteLine("Masukkan kalimat/kata pembanding : ");
                string input2 = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
                list.Add(Check(input, input2));
                kata--;
            }
            foreach(bool b in list)
            {
                Console.WriteLine(b ? "YES" : "NO");
            }
        }

       public static bool Check(string s , string s2)
       {
            for (int i = 0; i < s.Length; i++)
            {
                for (int j = 0; j < s2.Length; j++)
                {
                    if (s[i] == s2[j])
                        return true;
                }
            }
            return false;
       }
    }
}
