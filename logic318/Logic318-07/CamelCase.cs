﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_07
{
    public class CamelCase
    {
        public CamelCase()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            int count = 1;
            foreach(char c in kalimat)
            {
                if(c == char.ToUpper(c))
                    count++;
            }
            Console.WriteLine(count);
        }
    }
}
