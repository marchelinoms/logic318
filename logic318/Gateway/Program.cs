﻿namespace Gateway;
public class Program
{
    static void Main(string[] args)
    {
        Initializer();
    }
    static void Initializer()
    {

        Console.WriteLine("---Welcome To Batch 318---");
        string answer = "";
        while (answer.ToUpper() == "T" || answer.ToUpper() != "Y")
        {
            Console.Write("Pilih hari : ");
            int hari = int.Parse(Console.ReadLine());
            switch (hari)
            {
                case 5:
                    Logic318_05.Program program05 = new Logic318_05.Program();
                    break;
                case 6:
                    Logic318_06.Program program06 = new Logic318_06.Program();
                    break;
                case 7:
                    Logic318_07.Program program07 = new Logic318_07.Program();
                    break;
                case 8:
                    Logic318_08.Program program08 = new Logic318_08.Program();
                    break;
                case 9:
                    Logic318_09.Program program09 = new Logic318_09.Program();
                    break;
                case 10:
                    Logic318_10.Program program10 = new Logic318_10.Program();
                    break;
                default:
                    Console.WriteLine("Project belum tersedia");
                    break;
            }
            Console.Write("Keluar ? [Y/T] : ");
            answer = Console.ReadLine();
        }
    }
}