﻿using Logic318_04;

//abstractClass();
//objectClass();
//constructor();
//encapsulation();
//inheritance();
//overriding();

//rekursifDesc(); -->  Descending Order
//rekursifAsc();

static void rekursifAsc()
{
    Console.WriteLine("--Recursive Function (Ascending)---");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    int start = 1;

    perulangan(input,start);
}
static void rekursifDesc()
{
    Console.WriteLine("--Recursive Function (Descending)---");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());

    perulanganDesc(input);
}
static int perulangan(int input, int start)
{
    if(input == start)
    {
        Console.WriteLine($"{start}");
        return start;
    }
    Console.WriteLine($"{start}");
    return perulangan(input, start + 1);
}

static int perulanganDesc(int input)
{
    if (input == 1)
    {
        Console.WriteLine($"{input}");
        return input;
    }
    Console.WriteLine($"{input}");
    return perulanganDesc(input - 1);
}

static void overriding()
{
    Console.WriteLine("---Overriding---");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    kucing.pindah();
    paus.pindah();
}
static void inheritance()
{
    Console.WriteLine("---Inheritance---");
    TypeMobil civic = new TypeMobil();
    civic.Civic();
}
static void encapsulation()
{
    Console.WriteLine("---Encapsulation---");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 10;
    pp.lebar = 20;
    pp.tampilkanData();
}

static void constructor()
{
    Console.WriteLine("---Constructor---");
    Mobil mobil = new Mobil("B 413 I");
    string platno = mobil.getPlatNo();
    Console.WriteLine($"Mobil dengan nomor polisi : {platno}");
}
static void abstractClass()
{
    Console.WriteLine("---Abstract Class---");
    Console.WriteLine("Input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.WriteLine("Input y : ");
    int y = int.Parse(Console.ReadLine());

    Calculator calc = new Calculator();
    int jumlah = calc.jumlah(x, y);
    int kurang = calc.kurang(x, y);

    Console.WriteLine($"Hasil penjumlahan {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil pengurangan {x} - {y} = {kurang}");

}
static void objectClass() 
{
    Console.WriteLine("---Object Class---");
    Mobil mobil = new Mobil() { nama = "Ferrari", kecepatan = 0, bensin = 10,posisi=0};
    //Mobil mobil2 = new Mobil();
    //mobil2.nama = "Hyundai";
    //mobil2.kecepatan = 0;
    //mobil2.bensin = 10;
    //mobil2.posisi = 0;

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);
    mobil.utama();
}
