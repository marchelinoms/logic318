﻿
//KonversiTipeData();
//OperatorPenjumlahan();
//OperatorModulus();
//OperatorPerbandingan();
//OperatorLogika();
//CobaIf();
//NestedIf();
//ternary();
//manipulasiString();
//splitJoin();
subString();



//How to print something
static void Greetings() 
{
    Console.WriteLine("Hello Gaes");
    Console.WriteLine("Hello Gaes!!");
    Console.Write("HELLO GAES");
    Console.WriteLine("HELLO GAES!!!");
}

//How to read user's input
static void Inputan() 
{

    Console.Write("Masukkan nama : ");
    string nama = Console.ReadLine()!;
    //Ways to print the input
    Console.WriteLine("Hi, " + nama);//Concat 
    Console.WriteLine($"Hi, {nama}");//Using $ sign
    Console.WriteLine("Hi, {0}", nama);//Using index

}
//Variables
static void Variables() 
{
    //Explicit
    string alamat;//Declare without initilizing value
    string namaSamaran = "Haha";//Declare and initialize

    //Implicit (Must be initiated after declared/ value cannot be initiated later)
    var namaVariable = "Isi Variable";

    //Mutable (value can be replace)
    var age = 10;
    age = 21;

    //Immutable (value constant)
    const double PHI = 3.14;
}

//Data Type Conversion
static void KonversiTipeData() 
{

    int myInt = 10;
    double myDouble = 5.25;
    bool myBool = true;

    Console.WriteLine("Convert int to string    : " + myInt.ToString());//Convert int to string
    Console.WriteLine("Convert int to double    : " + Convert.ToDouble(myInt));//Convert int to double   
    Console.WriteLine("Convert double to string : " + Convert.ToString(myDouble));//Convert double to string   
    Console.WriteLine("Convert double to int    : " + Convert.ToInt32(myDouble));//Convert double to int   
    Console.WriteLine("Convert bool to string   : " + Convert.ToString(myBool));//Convert bool to string   
}

//Operator
static void OperatorPenjumlahan() 
{
    int mangga, apel, hasil = 0;

    Console.WriteLine("---Operator Aritmatika---");
    Console.Write("Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Apel = ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;
    Console.WriteLine($"Mangga + Apel = {hasil} ");
}
static void OperatorModulus() 
{
    int mangga, apel, hasil = 0;

    Console.WriteLine("---Modulus---");
    Console.Write("Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Apel = ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga % apel;
    Console.WriteLine($"Mangga % Apel = {hasil} ");
}

static void OperatorPenugasan() 
{
    int mangga = 10; 
    int apel =    8;

    //isi ulanga variable
    mangga = 15;
    Console.WriteLine("Mangga = " + mangga);

    apel += 6;
    Console.WriteLine("Apel = " + apel);
}
static void OperatorPerbandingan()
{
    int mangga, apel = 0;
    Console.WriteLine("---Opeartor Perbandingan---");

    Console.Write("Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Apel = ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine($"Mangga > Apel = {mangga > apel} ");
    Console.WriteLine($"Mangga >= Apel = {mangga >= apel} ");
    Console.WriteLine($"Mangga < Apel = {mangga < apel} ");
    Console.WriteLine($"Mangga <= Apel = {mangga <= apel} ");

}

static void OperatorLogika() 
{
    Console.WriteLine("---Opeartor Logika---");
    Console.Write("Masukkan Umur : ");
    int umur = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Password : ");
    string password = Console.ReadLine();
    
    bool isValid = password == "Admin" ? true : false;
    bool isAdult = umur > 18 ? true : false;

    if (isAdult && isValid)
        Console.WriteLine("Anda sudah dewasa dan password valid");
    else if (isAdult && !isValid)
        Console.WriteLine("Anda sudah dewasa dan password invalid");
    else if (!isAdult && isValid)
        Console.WriteLine("Anda belum dewasa dan password valid");
    else
        Console.WriteLine("Anda belum dewasa dan password invalid");
}

static void CobaIf() 
{
    Console.WriteLine("---Coba If---");
    int mtk, fisika, kimia , ratarata ;
    Console.Write("Masukkan Nilai MTK : ");
    mtk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Fisika : ");
    fisika = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Kimia : ");
    kimia = int.Parse(Console.ReadLine());

    ratarata = (mtk + fisika + kimia) / 3;
    bool isValid = mtk >=0 && fisika >= 0 && kimia >= 0 ? true : false;
    string hasil = ratarata > 50 ? "Selamat\nKamu Berhasil\nKamu Hebat" : "Maaf\nKamu gagal";

    if (isValid) 
    { 
        Console.WriteLine($"Nilai Rata-Rata : {ratarata}");
        Console.WriteLine(hasil);
    }
    else
        Console.WriteLine("Input Invalid");
}

static void NestedIf() 
{
    Console.WriteLine("---IMP Fashion---");
    int kodeBaju, harga = 0;
    string merkBaju = "",kodeUkuran;

    Console.Write("Masukkan Kode Baju   : ");
    kodeBaju = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kode Ukuran : ");
    kodeUkuran = Console.ReadLine().ToLower();

    if (kodeBaju == 1)
    {
        merkBaju = "IMP";
        if (kodeUkuran == "s")
            harga = 20000;
        else if (kodeUkuran == "m")
            harga = 220000;
        else
            harga = 250000;
    }
    else if (kodeBaju == 2)
    {
        merkBaju = "Prada";
        if (kodeUkuran == "s")
            harga = 150000;
        else if (kodeUkuran == "m")
            harga = 160000;
        else
            harga = 170000;
    }
    else if (kodeBaju == 3)
    {
        merkBaju = "Gucci";
        harga = 200000;
    }
    else
        Console.WriteLine("Input Invalid");

    Console.WriteLine($"Merk Baju : {merkBaju} \nHarga : {harga}");
}

static void ternary()
{
    Console.Write("Masukkan nilai x = ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y = ");
    int y = int.Parse(Console.ReadLine());

    string z = x > y ? "x lebih besar dari y" : "y lebih besar dari x";
    Console.WriteLine(z);

}

static void manipulasiString()
{
    Console.WriteLine("---Manipulasi String---");
    Console.Write("Masukkan kata : ");
    string input = Console.ReadLine();

    string remove = input;
    string insert = input;
    string replace = input;

    Console.WriteLine($"Panjang karakter dari {input} = {input.Length}");
    Console.WriteLine($"To Upper dari {input} = {input.ToUpper()}");
    Console.WriteLine($"To Lower dari {input} = {input.ToLower()}");
    
    Console.WriteLine($"Remove dari {remove} = {remove.Remove(11)}");
    Console.WriteLine($"Insert dari {insert} = {insert.Insert(5,"Banget")}");
    Console.WriteLine($"Replace dari {replace} = {replace.Replace("id","net")}");
}

static void splitJoin()
{
    Console.WriteLine("---Split & Join---");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();

    string[] arrayKata = kalimat.Split(" ");
    foreach (string item in arrayKata)
    {
        Console.WriteLine(item);
    }

    Console.WriteLine(string.Join(' ', arrayKata));
}

static void subString()
{
    Console.WriteLine("---Sub String---");

    Console.Write("Masukkan kode : ");
    string kode = Console.ReadLine();
    string tahun = kode.Substring(1, 4);
    string bulan = kode.Substring(5, 2);
    string lokasi = kode.Substring(7, 2);
    string nomor = kode.Substring(9);

    Console.WriteLine($"String Tahun    : {tahun}");
    Console.WriteLine($"String Bulan    : {bulan}");
    Console.WriteLine($"String Lokasi   : {lokasi}");
    Console.WriteLine($"String Nomor    : {nomor}");
}