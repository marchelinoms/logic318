﻿//Console.WriteLine($"Hasil Penjumlahan a + b = {Nilai()}");

namespace Logic318_05;

public class Program
{
    public Program()
    {
        MainMenu();   
        Console.ReadKey();
    }
    static void Main(string[] args)
    {
        MainMenu();
        Console.ReadKey();
    }
    static void MainMenu()
    {
        Console.WriteLine("---Day 5---");
        Console.WriteLine("1.Array 1 Dimensi");
        Console.WriteLine("2.Array 2 Dimensi");
        Console.Write("Pilihan : ");
        int pilihan = int.Parse(Console.ReadLine());
        switch (pilihan)
        {
            case 1:
                Menu();
                break;
            case 2:
                Menu2();
                break;
            default:
                Console.Clear();
                Console.WriteLine("Input Invalid");
                MainMenu();
                break;
        }
    }
    static void Menu()
    {
        Console.WriteLine("---Array 1 Dimensi---");
        int soal = 0;
        while (soal < 1)
        {
            Console.Write("Masukkan nomor soal : ");
            soal = int.Parse(Console.ReadLine());
        }
        switch (soal)
        {
            case 1:
                Soal01 soal01 = new Soal01();
                break;
            case 2:
                Soal02 soal02 = new Soal02();
                break;
            case 3:
                Soal03 soal03 = new Soal03();
                break;
            case 4:
                Soal04 soal04 = new Soal04();
                break;
            case 5:
                Soal05 soal05 = new Soal05();
                break;
            case 6:
                Soal06 soal06 = new Soal06();
                break;
            case 7:
                Soal07 soal07 = new Soal07();
                break;
            case 8:
                Soal08 soal08 = new Soal08();
                break;
            case 9:
                Soal09 soal09 = new Soal09();
                break;
            case 10:
                Soal10 soal10 = new Soal10();
                break;
            default:
                Console.Clear();
                Console.WriteLine("Input Invalid");
                Menu();
                break;
        }
        Console.WriteLine();
    }

    static void Menu2()
    {
        Console.WriteLine("---Array 2 Dimensi ---");
        int soal = 0;
        object ulang;
        while (soal < 1)
        {
            Console.Write("Masukkan nomor soal : ");
            soal = int.Parse(Console.ReadLine());
        }
        switch (soal)
        {
            case 1:
                Array2DSoal01 soal01 = new Array2DSoal01();
                break;
            case 2:
                Array2DSoal02 soal02 = new Array2DSoal02();
                break;
            case 3:
                Array2DSoal03 soal03 = new Array2DSoal03();
                break;
            case 4:
                Array2DSoal04 soal04 = new Array2DSoal04();
                break;
            case 5:
                Array2DSoal05 soal05 = new Array2DSoal05();
                break;
            case 6:
                Array2DSoal06 soal06 = new Array2DSoal06();
                break;
            case 7:
                Array2DSoal07 soal07 = new Array2DSoal07();
                break;
            case 8:
                Array2DSoal08 soal08 = new Array2DSoal08();
                break;
            case 9:
                Array2DSoal09 soal09 = new Array2DSoal09();
                break;
            case 10:
                Array2DSoal10 soal10 = new Array2DSoal10();
                break;
            default:
                Console.Clear();
                Console.WriteLine("Input Invalid");
                Menu2();
                break;
        }
    }
}
