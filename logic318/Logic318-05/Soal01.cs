﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_05
{
    public class Soal01
    {
        public Soal01()
        {
            //1  3   5   7   9   11  13
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            string[] array = new string[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = (i * 2 + 1).ToString();
            }
            Printing.Array1Dim(array);
        }
    }
    class Soal02
    {
        public Soal02()
        {
            //2	 4	6	8	10	12	14
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            string[] array = new string[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = ((i + 1) * 2).ToString();
            }
            Printing.Array1Dim(array);
        }
    }
    class Soal03
    {
        public Soal03()
        {
            //1	 4	7	10	13	16	19
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            string [] array = new string [n];
            for (int i = 0; i < n; i++)
            {
               array[i] = (i * 3 + 1).ToString();
            }
            Printing.Array1Dim(array);
        }
    }
    class Soal04
    {
        public Soal04()
        {
            // 1   5   9   13  17  21  25
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            string[] array = new string[n];
            for (int i = 0; i < n; i++)
            {
               array[i] =  (i * 4 + 1).ToString();
            }
            Printing.Array1Dim(array);
        }
    }
    class Soal05
    {
        public Soal05()
        {
            //  1 5 * 9 13 * 17
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            int a = 1;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                    Console.Write("*\t");
                a += 4;
                Console.Write($"{a}\t");
            }
        }
    }
    class Soal06
    {
        public Soal06()
        {
            //1	    5	*	13	17	*	25
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            //int a = 1;
            for (int i = 1; i <= n; i++)
            {
                //if (i % 3 == 0)
                //{
                //    Console.Write("*\t");
                //    //a += 4;
                //    //continue;
                //}
                //else
                //{
                //    Console.Write($"{i*4-3}\t");
                //}

                Console.Write(i % 3 != 0 ? $"{ i * 4 - 3 }\t" : "*\t");
                //a += 4;
            }
        }
    }
    class Soal07
    {
        public Soal07()
        {
            //2	 4	8   16	32	64	128 
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{Math.Pow(2, i)}\t");
            }
        }
    }
    class Soal08
    {
        public Soal08()
        {
            //3	9	27	81	243	729	2187
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{Math.Pow(3, i)}\t");
            }
        }
    }
    class Soal09
    {
        public Soal09()
        {
            //4	16	*	64	256	*	1024
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            int a = 4;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("*\t");
                }
                else
                {
                    Console.Write($"{a}\t");
                    a *= 4;
                }
            }
        }
    }
    class Soal10
    {
        public Soal10()
        {
            //3	 9	27	XXX	243	729	2187
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.Write(i % 4 == 0 ? $"{DigitToString(Math.Pow(3, i))}\t" : $"{Math.Pow(3, i)}\t");
            }
        }
        private string DigitToString(double digit)
        {
            string result = "";
            for (int i = 0; i < digit.ToString().Length; i++)
            {
                result += "X";
            }
            return result;
        }
    }
}
