﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_05
{
    public class Array2D
    {

    }
    class Array2DSoal01
    {
        //        n = 7, n2 = 3

        //i :	0	1	2	3	4	5	6
        //0 :	1	3	9	27	81	243	729
        public Array2DSoal01()
        {
            Console.WriteLine("---Soal 01---");
            int n1, n2;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());
            Console.Write("n2 = ");
            n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[2, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (Math.Pow(n2, i)).ToString();
            }
            Printing.Array2Dim(array);
        }

    }
    class Array2DSoal02
    {
        public Array2DSoal02()
        {

            Console.WriteLine("---Soal 02---");
            int n1, n2;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());
            Console.Write("n2 = ");
            n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[2, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (i + 1) % 3 == 0 ? $"{Math.Pow(n2, i) * -1}" : $"{Math.Pow(n2, i)}";
            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal03
    {
        public Array2DSoal03()
        {

            Console.WriteLine("---Soal 03---");
            int n1, n2;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());
            Console.Write("n2 = ");
            n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[2, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                if (i <= n1 / 2)
                {
                    array[1, i] = n2.ToString();
                    array[1, n1 - 1 - i] = n2.ToString();
                }
            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal04
    {
        public Array2DSoal04()
        {

            Console.WriteLine("---Soal 04---");
            int n1, n2, a = 0;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());
            Console.Write("n2 = ");
            n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[2, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                if (i % 2 == 0)
                {
                    array[1, i] = n2.ToString();
                    a++;
                }
                else
                    array[1, i] = (n2 * a).ToString();
            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal05
    {
        public Array2DSoal05()
        {

            Console.WriteLine("---Soal 05---");
            int n1;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());

            string[,] array = new string[3, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (n1 + i).ToString();
                array[2, i] = (n1 * 2 + i).ToString();

            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal06
    {
        public Array2DSoal06()
        {
            //            n = 7

            //0   1   2   3   4   5   6
            //1   7   49  343 2401    16807   117649
            //1   8   51  346 2405    16812   117655
            Console.WriteLine("---Soal 06---");
            int n1;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());

            string[,] array = new string[3, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (Math.Pow(n1, i)).ToString();
                array[2, i] = (Math.Pow(n1, i) + i).ToString();

            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal07
    {
        public Array2DSoal07()
        {
            //            n = 7

            //0   1   2   3   4   5   6
            //7   8   9   10  11  12  13
            //14  15  16  17  18  19  20
            Console.WriteLine("---Soal 07---");
            int n1;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());

            string[,] array = new string[3, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (n1 + i).ToString();
                array[2, i] = (n1 * 2 + i).ToString();

            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal08
    {
        public Array2DSoal08()
        {
            //            n = 7

            //0   1   2   3   4   5   6
            //0   2   4   6   8   10  12
            //0   3   6   9   12  15  18
            Console.WriteLine("---Soal 08---");
            int n1;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());

            string[,] array = new string[3, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (n1 * 2).ToString();
                array[2, i] = (n1 * 3).ToString();

            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal09
    {
        public Array2DSoal09()
        {
            //            n = 7, n2 = 3

            //0   1   2   3   4   5   6
            //0   3   6   9   12  15  18
            //18  15  12  9   6   3   0
            Console.WriteLine("---Soal 09---");
            int n1, n2;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());
            Console.Write("n2 = ");
            n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[3, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (i * n2).ToString();
                array[2, i] = ((n1 - 1 - i) * n2).ToString();

            }
            Printing.Array2Dim(array);
        }
    }
    class Array2DSoal10
    {
        public Array2DSoal10()
        {
            //            n = 7, n2 = 3

            //            0   1   2   3   4   5   6
            //            0   3   6   9   12  15  18
            //            0   4   8   12  16  20  24

            Console.WriteLine("---Soal 10---");
            int n1, n2;
            Console.Write("n1 = ");
            n1 = int.Parse(Console.ReadLine());
            Console.Write("n2 = ");
            n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[3, n1];
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (i * n2).ToString();
                array[2, i] = (i * (n2 + 1)).ToString();
            }
            Printing.Array2Dim(array);
        }
    }
}
