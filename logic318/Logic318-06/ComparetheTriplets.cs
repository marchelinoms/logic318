﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_06
{
    public class ComparetheTriplets
    {
        public ComparetheTriplets()
        {
            Console.Write("Masukkan Skor Alice : ");
            int[] alice = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("Masukkan Skor Bob : ");
            int[] bob = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int[] result = CompareArray(alice, bob);
            Printing.Array1Dim(result);
        }
        public int[] CompareArray(int[] a, int[] b)
        {
            int p1 = 0, p2 = 0;
            int[] result = new int[2];
            for(int i = 0 ; i < a.Length; i++)
            {
                if (a[i] > b[i])
                    p1++;
                if (a[i] < b[i])
                    p2++;
            }
            result[0]= p1;
            result[1]= p2;
            return result;
        }
    }
}
