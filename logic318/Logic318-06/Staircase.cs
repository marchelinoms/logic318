﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class Staircase
    {
        public Staircase()
        {
            Console.Write("Masukkan Input : ");
            int n = int.Parse(Console.ReadLine());
            
            for(int i = n-1; i >= 0; i--)
            {
                for(int j = 0; j < n ; j++)
                {
                    if (j <= i)
                        Console.Write("#");
                    else
                        Console.Write(" ");
                }
                Console.WriteLine("");
            }
        }
    }
}
