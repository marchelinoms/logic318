﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class SimpleArraySum
    {
        public SimpleArraySum()
        {
            Console.Write("Masukkan barisan angka : ");
            int [] arr = Array.ConvertAll<string,int>(Console.ReadLine().Split(' '),int.Parse);
            Console.WriteLine(Sum(arr));
        }
        public int Sum(int[] arr) 
        {
            int result = 0;
            for (int i = 0; i < arr.Length; i++)
                result += arr[i];
            return result;
        }

    }
}
