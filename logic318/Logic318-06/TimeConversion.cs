﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class TimeConversion
    {
        public TimeConversion()
        {
            Console.Write("Input Time [hh:mm:ss AM/PM] : ");
            string input = Console.ReadLine();
            input = String.Concat(input.Where(a => !char.IsWhiteSpace(a)));
            Console.WriteLine(FormatConverter(input));
        }
        public string FormatConverter(string input)
        {
            int hour = Convert.ToInt32(input.Substring(0, 2));
            string format = input.Substring(8, 2).ToUpper();
            string result = "";
            if (format == "PM" && hour <= 12)
                result = hour == 12 ? input.Substring(0, 8) : $"{hour + 12}{input.Substring(2, 6)}";
            else if (format == "AM" && hour <= 12)
                result = hour == 12 ? $"00{input.Substring(2, 6)}" : input.Substring(0, 8);

            return result;
        }
    }
}
