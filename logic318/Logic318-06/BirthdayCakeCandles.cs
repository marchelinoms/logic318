﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class BirthdayCakeCandles
    {
        public BirthdayCakeCandles()
        {
            Console.Write("Masukkan input : ");
            int[] arr = Array.ConvertAll<string, int>(Console.ReadLine().Split(" "), int.Parse);
            int count = 0;
            Array.Sort(arr);
           
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != arr[arr.Length - 1])
                    continue;

                count++;
            }
            Console.WriteLine(count);
        }
    }
}
