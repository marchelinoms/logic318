﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class PlusMinus
    {
        public PlusMinus()
        {
            Console.WriteLine("Masukkan barisan angka : ");
            int[] arr = Array.ConvertAll<string,int>(Console.ReadLine().Split(' '), int.Parse);
            Console.Write("\n");
            Console.WriteLine(Proportion(arr));
        }
        public string Proportion(int [] arr)
        {
            double pos = 0, neg = 0, zero = 0;
            for(int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > 0)
                    pos++;
                if (arr[i] < 0)
                    neg++;
                if(arr[i] == 0)
                    zero++;
            }
            string result = $"Positive:{pos / arr.Length}\nNegative:{(neg / arr.Length)}\nZero:{zero / arr.Length}";
            return result;
        }
    }
}
