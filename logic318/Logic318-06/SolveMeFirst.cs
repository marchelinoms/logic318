﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class SolveMeFirst
    {
        public SolveMeFirst()
        {
            int a, b, result;
            Console.Write("Masukkan nilai a: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b: ");
            b = int.Parse(Console.ReadLine());

            result = Addition(a,b);
            Console.WriteLine(result);
        }
        public int Addition(int a, int b)
        {
            return a + b;
        }
        
    }
}
