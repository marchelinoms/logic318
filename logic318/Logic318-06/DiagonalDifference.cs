﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic318_06
{
    public class DiagonalDifference
    {
        public DiagonalDifference()
        {
            int n = 0, a = 0, b = 0;
            Console.Write("n  : ");
            n = int.Parse(Console.ReadLine());

            int[,] matrix = new int[n, n];
            Console.WriteLine("Masukkan barisan angka  : ");
            for (int i = 0; i < n; i++)
            {
                int[] arr = Array.ConvertAll<string, int>(Console.ReadLine().Split(' '), int.Parse);
                for (int j = 0; j < n; j++)
                {
                    if (i == j)
                        a += arr[j];
                    if (i + j == n - 1)
                        b += arr[j];
                    matrix[i, j] = arr[j];
                }
            }
            Console.WriteLine();
            Printing.Array2Dim(matrix);
            Console.WriteLine($"Diagonal Difference = {Math.Abs(a-b)} ");
        }
    }
}
