﻿namespace Logic318_06;

public class Program
{
    public Program()
    {
        Menu();
    }
    static void Main(string[] args)
    {
        Menu();
    }
    static void Menu()
    {
        int soal = 0;
        Console.WriteLine("---Day 06---");
        while (soal < 1)
        {
            Console.WriteLine("---1. Solve Me First---");
            Console.WriteLine("---2. Time Conversion---");
            Console.WriteLine("---3. Simple Array Sum---");
            Console.WriteLine("---4. Diagonal Difference---");
            Console.WriteLine("---5. Plus Minus---");
            Console.WriteLine("---6. Staircase---");
            Console.WriteLine("---7. Min-Max Sum---");
            Console.WriteLine("---8. Birthday Cake Candles---");
            Console.WriteLine("---9.Compare the Triplets---");
            Console.WriteLine("Masukkan no soal: ");
            soal = int.Parse(Console.ReadLine());
        }
        switch (soal)
        {
            case 1:
                SolveMeFirst solveMeFirst = new SolveMeFirst();
                break;
            case 2:
                TimeConversion timeConversion = new TimeConversion();
                break;
            case 3:
                SimpleArraySum simpleArraySum = new SimpleArraySum();
                break;
            case 4:
                DiagonalDifference diagonalDifference = new DiagonalDifference();
                break;
            case 5:
                PlusMinus plusMinus = new PlusMinus();
                break;
            case 6:
                Staircase staircase = new Staircase();
                break;
            case 7:
                Min_MaxSum mini_MaxSum = new Min_MaxSum();
                break;
            case 8:
                BirthdayCakeCandles birthdayCakeCandles = new BirthdayCakeCandles();
                break;
            case 9:
                ComparetheTriplets comparetheTriplets = new ComparetheTriplets();
                break;
        }

    }
}
