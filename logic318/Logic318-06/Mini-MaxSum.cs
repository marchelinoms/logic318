﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_06
{
    public class Min_MaxSum
    {
        public Min_MaxSum()
        {
            Console.Write("Masukkan barisan angka : ");
            int[] arr = Array.ConvertAll<string, int>(Console.ReadLine().Split(" "), int.Parse);
            int[] res = new int[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (i == j) continue;
                    res[i] += arr[j];
                }
            }
            Array.Sort(res);
            Console.WriteLine($"{res[0]} {res[res.Length - 1]}");
        }

    }
}
