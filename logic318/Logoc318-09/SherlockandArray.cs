﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_09
{
    public class SherlockandArray
    {
        // 1 2 3 -> NO
        public SherlockandArray()
        {
            Console.Write("Input : ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(' '),int.Parse);
            Console.WriteLine(balancedSums(input));
        }
        public static string balancedSums(int[] input)
        {
            bool isTrue = false;
            int sumLeft = 0, sumRight = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (i == 0)
                {
                    sumLeft = 0;
                    sumRight = Sum(input, i + 1, input.Length - 1);
                }
                else if (i == input.Length - 1)
                {
                    sumRight = 0;
                    sumLeft = Sum(input, 0, i - 1);
                }
                else
                {
                    sumLeft = Sum(input, 0, i - 1);
                    sumRight = Sum(input, i + 1, input.Length - 1);
                }

                if (sumLeft == sumRight)
                {
                    isTrue = true;
                    break;
                }
            }
           return isTrue ? "Yes" : "No";
        }
        private static int Sum(int[] arr, int left, int right)
        {
            int sum = 0;
            for (int i = left; i < right; i++)
            {
                sum++;
            }

            return sum;
        }
    }
}
