﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_09
{
    internal class DateTimeReview
    {
        public DateTimeReview()
        {
            // format datetime yyyy-MM-ddThh:mm:ss
            Console.WriteLine("DateTime Review");
            Console.WriteLine("Tanggal awal");
            string[] dtAwal = Console.ReadLine().Split('T');
            string[] dateAwal = dtAwal[0].Split('-');
            string[] timeAwal = dtAwal[1].Split(':');
            DateTime awal = new DateTime(int.Parse(dateAwal[0]), int.Parse(dateAwal[1]), int.Parse(dateAwal[2]), int.Parse(timeAwal[0]), int.Parse(timeAwal[1]), int.Parse(timeAwal[2]));
            Console.WriteLine("Tanggal akhir");
            string[] dtAkhir = Console.ReadLine().Split('T');
            string[] dateAkhir = dtAkhir[0].Split('-');
            string[] timeAkhir = dtAkhir[1].Split(':');
            DateTime akhir = new DateTime(int.Parse(dateAkhir[0]), int.Parse(dateAkhir[1]), int.Parse(dateAkhir[2]), int.Parse(timeAkhir[0]), int.Parse(timeAkhir[1]), int.Parse(timeAkhir[2]));

            TimeSpan diff = akhir - awal;
            Console.WriteLine($"{awal.ToString("yyyy-MM-ddThh:mm:ss")} - {akhir.ToString("yyyy-MM-ddThh:mm:ss")}");

            Console.WriteLine($"Selisih hari {diff.Days}");
            Console.WriteLine($"Selisih jam {diff.Hours}");
            Console.WriteLine($"Selisih meint {diff.Minutes}");
            Console.WriteLine($"Selisih detik {diff.Seconds}");
        }
    }
}
