﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic318_09
{
    public class MissingNumbers
    {
        public MissingNumbers()
        {
            Console.Write("Barisan angka pertama = ");
            List<int> list1 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse).ToList();
            Console.Write("Barisan angka kedua = ");
            List<int> list2 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse).ToList(); 
            
            foreach(int i in NumbersMiss(list1, list2))
            {
                Console.Write($"{i}\t");
            }
        }
        // 1 2 3 4 5
        // 2 3 4 5 6 2
        // Hasil : 1 2 6
        public static List<int> NumbersMiss(List<int> list1, List<int> list2)
        {
            List<int> result = new List<int>();
            for(int i = 0; i < list1.Count; i++) 
            {
                for(int j = 0; j < list2.Count; j++)
                {
                    if (list1[i] == list2[j])
                    {
                        list1.RemoveAt(i);
                        list2.RemoveAt(j);
                        i--;j--;
                        break;
                    }
                }
            }

            foreach(int i in list1 )
            {
                result.Add(i);
            }
            foreach(int i in list2 )
            {
                result.Add(i);
            }

            result.Sort();
            return result.Distinct().ToList();
        }
    }
}
